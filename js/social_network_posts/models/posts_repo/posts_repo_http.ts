import { Post } from "../post/post";
import { PostsRepo } from "./posts_repo";
import { SaveNewPostRequest, SaveNewPostResponse, GetAllPostsRequest, GetAllPostsResponse, DeletePostRequest, InteractWithPostRequest, InteractWithPostResponse } from '@social_network/network_contracts'
import { HttpRequester } from '@social_network/http_requester'

export const PostsRepoHttp = ({ httpRequester }: { httpRequester: HttpRequester }): PostsRepo => {
    return {
        getAll: async (paginationKey: string, pageSize: number) => {
            const requestBody: GetAllPostsRequest = {
                paginationKey,
                pageSize,
            };

            const response = await httpRequester.get<GetAllPostsResponse>({
                path: '/social-network-posts',
                queryParams: requestBody
            });

            return {
                nextPaginationKey: response.nextPaginationKey,
                posts: response.data.map(post => ({
                    userId: post.userId,
                    createdAt: post.createdAt,
                    userNickName: post.userNickName,
                    text: post.text,
                    interactionCount: post.interactionCount,
                    hasInteractionByUser: post.hasInteractionByUser,
                })),
            }
        },
        save: async (text: string): Promise<Post> => {
            const requestBody: SaveNewPostRequest = {
                text
            };

            const response: SaveNewPostResponse = await httpRequester.post<SaveNewPostResponse>({
                path: '/social-network-posts',
                body: requestBody
            });

            return {
                userId: response.data.userId,
                createdAt: response.data.createdAt,
                userNickName: response.data.userNickName,
                text: response.data.text,
                hasInteractionByUser: false,
                interactionCount: 0
            }
        },
        delete: async (post: Post) => {
            const requestBody: DeletePostRequest = {
                createdAt: post.createdAt
            };

            await httpRequester.delete({
                path: '/social-network-posts',
                body: requestBody
            });
        },
        interact: async (post: Post, isPositiveInteraction: boolean) => {
            const requestBody: InteractWithPostRequest = {
                isPositiveInteraction,
                postUserId: post.userId,
                postCreatedAt: post.createdAt
            };

            const response = await httpRequester.post<InteractWithPostResponse>({
                path: '/social-network-posts/interact',
                body: requestBody
            });

            return {
                userId: response.data.userId,
                createdAt: response.data.createdAt,
                userNickName: response.data.userNickName,
                text: response.data.text,
                hasInteractionByUser: response.data.hasInteractionByUser,
                interactionCount: response.data.interactionCount
            }
        },
    }
}