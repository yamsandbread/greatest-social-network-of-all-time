import { Post } from "../post/post";

export interface PostsRepo {
    getAll(paginationKey: string, pageSize: number): Promise<{ posts: Post[], nextPaginationKey: string }>
    save(text: string): Promise<Post>
    delete(post: Post): Promise<void>
    interact(post: Post, isPositiveInteraction: boolean): Promise<Post>
}