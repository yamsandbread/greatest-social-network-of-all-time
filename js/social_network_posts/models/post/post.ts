export interface Post {
    userId: string
    createdAt: string
    userNickName: string
    text: string
    interactionCount: number
    hasInteractionByUser: boolean
}