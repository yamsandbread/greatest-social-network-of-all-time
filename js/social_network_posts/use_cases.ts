import { HttpRequester } from '@social_network/http_requester'
import { Post } from './models/post/post';
import { PostsRepoHttp } from './models/posts_repo/posts_repo_http';

export type PostsUseCases = ReturnType<typeof getPostsUseCases>;

export const getPostsUseCases = ({ httpRequester }: { httpRequester: HttpRequester }) => {
    const postsRepo = PostsRepoHttp({ httpRequester })
    return {
        savePost: async (text: string) => {
            return postsRepo.save(text);
        },
        getAll: async (paginationKey: string, pageSize: number) => {
            return postsRepo.getAll(paginationKey, pageSize);
        },
        deletePost: async (post: Post) => {
            return postsRepo.delete(post);
        },
        interactWithPost: async (post: Post, isPositiveInteraction: boolean) => {
            return postsRepo.interact(post, isPositiveInteraction);
        }
    }
};