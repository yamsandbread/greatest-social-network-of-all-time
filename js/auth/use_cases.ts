import { LoginHandler } from "./models/login_handler/login_handler";
import { LoginHandlerFirebase } from "./models/login_handler/login_handler_firebase";
import { UsersRepoHttp } from "./models/users_repo/users_repo_http";
import { HttpRequester } from "@social_network/http_requester";

export const createNewLoginHandler = (configs: Record<string, string>) => new LoginHandlerFirebase(configs) as LoginHandler;

export type UsersUseCases = ReturnType<typeof getUsersUseCases>;

export const getUsersUseCases = ({ httpRequester }: { httpRequester: HttpRequester }) => {
    const repo = UsersRepoHttp({ httpRequester })
    return {
        getUser: async () => {
            return repo.get();
        }
    }
};
