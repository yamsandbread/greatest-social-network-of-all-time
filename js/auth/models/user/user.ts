export interface User {
    id: string,
    nickName: string,
    isNew: boolean,
}