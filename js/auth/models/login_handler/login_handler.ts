export interface LoginHandler {
    setup: () => Promise<void>
    getToken: () => Promise<string>
    login: () => Promise<string>
    isLoggedIn: () => boolean
}
