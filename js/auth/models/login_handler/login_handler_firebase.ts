import { getAuth, GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import { FirebaseApp, initializeApp } from 'firebase/app';

import { LoginHandler } from "./login_handler";

export class LoginHandlerFirebase implements LoginHandler {
    private app?: FirebaseApp;
    private provider?: GoogleAuthProvider;

    constructor(private configs: Record<string, string>) { };

    async setup() {
        this.app = initializeApp(this.configs);
        this.provider = new GoogleAuthProvider();

        const auth = getAuth();
        await new Promise((resolve, reject) => {
            auth.onAuthStateChanged((user) => {
                resolve(!!user);
            });
        })
    }

    async getToken() {
        const auth = getAuth();
        if (!auth.currentUser) {
            throw new Error("Attempted to get token before signing in");
        }
        return auth.currentUser.getIdToken()
    }

    isLoggedIn(): boolean {
        const auth = getAuth();
        return !!auth.currentUser;
    }

    async login(): Promise<string> {
        if (!this.provider) {
            throw new Error("Attempted to login before setup");
        }
        const auth = getAuth();
        const result = await signInWithPopup(auth, this.provider);
        return result.user.getIdToken();
    }

}