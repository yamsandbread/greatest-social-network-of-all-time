import { User } from "../user/user";

export interface UsersRepo {
    get: () => Promise<User>
}