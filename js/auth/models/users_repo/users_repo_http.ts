import { InitializeUserResponse } from '@social_network/network_contracts'
import { HttpRequester } from '@social_network/http_requester'
import { User } from "../user/user";
import { UsersRepo } from './users_repo';

export const UsersRepoHttp = ({ httpRequester }: { httpRequester: HttpRequester }): UsersRepo => {
    return {
        get: async (): Promise<User> => {
            const response = await httpRequester.post<InitializeUserResponse>({
                path: '/initialize-user',
            });

            return {
                id: response.data.id,
                nickName: response.data.nickName,
                isNew: response.data.isNew,
            }
        }
    }
}