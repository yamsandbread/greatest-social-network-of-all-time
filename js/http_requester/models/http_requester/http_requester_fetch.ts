import { HttpRequester } from "./http_requester";

export const HttpRequesterFetch = ({ baseUrl, tokenFunc }: {
    baseUrl: string,
    tokenFunc: () => Promise<string | undefined>
}): HttpRequester => {

    const getBaseFetchConfigs = async () => {
        const token = await tokenFunc();
        const initHeaders = new Headers();

        initHeaders.set('Content-Type', 'application/json; charset=UTF-8');
        initHeaders.set('Authorization', token ? `Bearer ${token}` : "");
        return {
            headers: initHeaders,
            mode: 'cors',
        } as RequestInit
    }

    return {
        get: async <T>(payload: { path: string, queryParams?: Record<string, any> }): Promise<T> => {
            const baseConfigs = await getBaseFetchConfigs();
            const urlSearchParams = new URLSearchParams(payload.queryParams);
            const urlQuery = urlSearchParams.toString() ? `${urlSearchParams}` : '';
            const result: T = await fetch(`${baseUrl}${payload.path}?` + urlQuery, {
                method: 'GET',
                ...baseConfigs
            }).then(async res => {
                if (!res.ok) {
                    throw await res.json();
                }
                return res.json();
            });
            return result;

        },
        post: async <T>(payload: { path: string, body?: Record<string, any> }): Promise<T> => {
            const baseConfigs = await getBaseFetchConfigs();
            const result: T = await fetch(`${baseUrl}${payload.path}`, {
                method: 'POST',
                body: payload.body ? JSON.stringify(payload.body) : undefined,
                ...baseConfigs
            }).then(async res => {
                if (!res.ok) {
                    throw await res.json();
                }
                return res.json();
            });
            return result;

        },
        put: async <T>(payload: { path: string, body?: Record<string, any> }): Promise<T> => {
            const baseConfigs = await getBaseFetchConfigs();
            const result: T = await fetch(`${baseUrl}${payload.path}`, {
                method: 'PUT',
                body: payload.body ? JSON.stringify(payload.body) : undefined,
                ...baseConfigs
            }).then(async res => {
                if (!res.ok) {
                    throw await res.json();
                }
                return res.json();
            });
            return result;

        },
        delete: async <T>(payload: { path: string, body?: Record<string, any> }): Promise<T> => {
            const baseConfigs = await getBaseFetchConfigs();
            const result: T = await fetch(`${baseUrl}${payload.path}`, {
                method: 'DELETE',
                body: payload.body ? JSON.stringify(payload.body) : undefined,
                ...baseConfigs
            }).then(async res => {
                if (!res.ok) {
                    throw await res.json();
                }
                return res.json();
            });
            return result;

        },
    }
}