export interface HttpRequester {
    get<T>(payload: { path: string, queryParams?: Record<string, any> }): Promise<T>
    post<T>(payload: { path: string, body?: Record<string, any> }): Promise<T>
    put<T>(payload: { path: string, body?: Record<string, any> }): Promise<T>
    delete<T>(payload: { path: string, body?: Record<string, any> }): Promise<T>
}