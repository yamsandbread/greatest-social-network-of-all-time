import { HttpRequesterFetch } from "./models/http_requester/http_requester_fetch";

export const createNewHttpRequester = (configs: {
    baseUrl: string,
    tokenFunc: () => Promise<string | undefined>
}) => HttpRequesterFetch(configs);