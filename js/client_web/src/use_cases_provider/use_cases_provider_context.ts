import { LoginHandler, UsersUseCases } from "@social_network/auth";
import { LearnerUseCases } from "@social_network/learner";
import { PostsUseCases } from "@social_network/social_network_posts";
import { createContext, useContext } from "react";

export const UseCasesProviderContext = createContext<{
    loginHandler: LoginHandler,
    postsUseCases: PostsUseCases
    usersUseCases: UsersUseCases
    learnerUseCases: LearnerUseCases
}>(undefined as any);

export function useUseCasesProvider() {
    return useContext(UseCasesProviderContext);
}
