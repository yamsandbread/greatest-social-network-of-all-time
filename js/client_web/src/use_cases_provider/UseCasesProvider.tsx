import { createNewLoginHandler, getUsersUseCases } from "@social_network/auth";
import { createNewHttpRequester } from "@social_network/http_requester";
import { getPostsUseCases } from "@social_network/social_network_posts";
import { Spin, theme } from "antd";
import { ReactNode, useState, Context, useEffect } from "react";
import { UseCasesProviderContext } from "./use_cases_provider_context";
import classes from './UseCasesProvider.module.css'
import { getLearnerUseCases } from "@social_network/learner";

interface Props {
    children: ReactNode
}

export default function UseCasesProvider({ children }: Props) {
    const { token } = theme.useToken();

    type ContextValue<T> = T extends Context<infer E> ? E : never;
    const [contextValue, setContextValue] = useState<ContextValue<typeof UseCasesProviderContext>>();

    useEffect(() => {

        (async () => {
            const loginConfigs = JSON.parse(atob(import.meta.env.VITE_FIREBASE_CONFIG));
            const loginHandler = createNewLoginHandler(loginConfigs);
            await loginHandler.setup();


            const httpRequester = createNewHttpRequester({
                baseUrl: import.meta.env.VITE_BASE_URL,
                tokenFunc: async () => {
                    if (!loginHandler.isLoggedIn()) return undefined;
                    return loginHandler.getToken();
                }
            });

            const postsUseCases = getPostsUseCases({ httpRequester })
            const usersUseCases = getUsersUseCases({ httpRequester });
            const learnerUseCases = getLearnerUseCases({ httpRequester });

            setContextValue({
                loginHandler,
                postsUseCases,
                usersUseCases,
                learnerUseCases
            })
        })();
    }, []);


    if (!contextValue) {
        return <div className={classes.loadingSectionWrapper}>
            <div className={classes.loadingSection} style={{ background: token.colorBgContainer }}>
                <div className={classes.loadingText}>One sec, checking authentication status.</div>
                <Spin />
            </div>
        </div>
    } else {
        return <UseCasesProviderContext.Provider value={contextValue}>
            {children}
        </UseCasesProviderContext.Provider>

    }
}
