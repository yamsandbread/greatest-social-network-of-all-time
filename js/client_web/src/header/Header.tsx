import { theme } from 'antd'
import classes from './Header.module.css'
import socialNetworkerIcon from '../assets/social_networker.svg';
import AddNewPostButton from '../social_network_post/AddNewPostButton';


export default function Header() {
    const { token } = theme.useToken();
    return <div className={classes.base} style={{ background: token.colorBgContainer }}>
        <img className={classes.logo} src={socialNetworkerIcon} alt="Icon" />
        <AddNewPostButton />
    </div>
}
