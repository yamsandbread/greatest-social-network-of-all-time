import { ConfigProvider } from "antd";
import { ReactNode } from "react";

interface Props {
    children: ReactNode
}

export default function LearnerTheme({ children }: Props) {
    return <ConfigProvider theme={{
        token: {
            colorPrimary: "#E26D5A"
        }
    }}>
        {children}
    </ConfigProvider>
}