import { RocketOutlined } from "@ant-design/icons"
import { Button } from "antd"
import { useState } from "react"
import CheckDeployModal from "./CheckDeployModal"

export default function CheckDeployButton() {
    const [isOpen, setIsOpen] = useState(false);

    return <>
        <Button
            type='primary'
            size='small'
            icon={<RocketOutlined />}
            onClick={() => setIsOpen(true)}
        >Verify Deploy</Button>
        <CheckDeployModal
            isOpen={isOpen}
            close={() => setIsOpen(false)}
        />
    </>
}