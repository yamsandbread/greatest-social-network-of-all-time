import { ExportOutlined } from "@ant-design/icons";
import { Space, Typography } from "antd";

interface Props {
    text: string
    url: string
}

export default function IconLink({ text, url }: Props) {
    return <Typography.Link href={url}>
        <Space>
            {text} <ExportOutlined />
        </Space>
    </Typography.Link>
}

