import { Button, Input, Modal, Spin, theme, Typography } from "antd";
import LearnerTheme from "../LearnerTheme";
import SocialNetworkPostBase from "../social_network_post/SocialNetworkPostBase";
import learnerIcon from '../assets/learner.svg';
import classes from './CheckDeployModal.module.css';
import { getPublicCiMutation } from "./queries/get_public_ci_mutation";
import { useState } from "react";

interface Props {
    isOpen: boolean
    close: () => void
}

export default function CheckDeployModal({ isOpen, close }: Props) {
    return <LearnerTheme>
        <Modal
            open={isOpen}
            onCancel={close}
            footer={<></>}
        >
            <ModalContent />
        </Modal>
    </LearnerTheme>
}

function ModalContent() {
    const { token } = theme.useToken();
    const [inputText, setInputText] = useState("");
    const { data, isLoading, isError, error, mutate, isSuccess } = getPublicCiMutation();

    return <SocialNetworkPostBase
        Logo={<img width={32} style={{ padding: '8px' }} src={learnerIcon} alt="Icon" />}
        Header={<Typography.Title className={classes.modalTitle}>Tech Checklist</Typography.Title>}
        Side={<div style={{ width: '48px', minWidth: '48px', padding: 'auto' }}>
            <div style={{ width: '1px', height: '100%', margin: 'auto', borderLeftColor: token.colorPrimary, borderLeftWidth: '1px', borderLeftStyle: 'solid' }} />
        </div>}
        Body={
            <div>
                <Typography.Paragraph>Check if your deploy worked by typing in the name of the JSON file you added.</Typography.Paragraph>
                <div className={classes.fileNameInputRow}>
                    <Input
                        className={classes.input}
                        value={inputText}
                        onChange={(event) => setInputText(event.target.value)}
                        placeholder="Name of your JSON file"
                    />
                    <Typography.Paragraph>.json</Typography.Paragraph>
                </div>
            </div>
        }
        Footer={<div className={classes.footer}>
            <Button
                className={classes.footerButton}
                type="primary"
                disabled={!inputText || isLoading}
                onClick={() => mutate(inputText)}
            >
                Check Response
            </Button>
            <div>
                {isLoading
                    ? <Spin />
                    : isError
                        ? <div>
                            <Typography.Paragraph className={classes.responseStatusText}>ERROR. The call failed with this response:</Typography.Paragraph>
                            <Typography.Paragraph>{JSON.stringify(error)}</Typography.Paragraph>
                        </div>
                        : isSuccess && data
                            ? <div>
                                <Typography.Paragraph className={classes.responseStatusText}>Success! This means your deploy worked. Here is the response:</Typography.Paragraph>
                                <Typography.Paragraph className={classes.successResponse}>{JSON.stringify(data)}</Typography.Paragraph>
                            </div>
                            : <></>
                }
            </div>
        </div>}
    />;
}
