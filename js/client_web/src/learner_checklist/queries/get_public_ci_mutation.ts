import { useMutation } from "react-query";
import { useUseCasesProvider } from "../../use_cases_provider/use_cases_provider_context";

export const getPublicCiMutation = () => {
    const { learnerUseCases } = useUseCasesProvider();
    return useMutation((fileName: string) => learnerUseCases.getPublicCi(fileName));
}
