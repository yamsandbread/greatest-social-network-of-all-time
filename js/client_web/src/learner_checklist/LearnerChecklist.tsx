import { CodeOutlined } from "@ant-design/icons";
import { Checkbox, FloatButton, Space, theme, Typography } from "antd";
import { ReactNode, useState } from "react";
import SocialNetworkPostBase from "../social_network_post/SocialNetworkPostBase";
import classes from './LearnerChecklist.module.css';
import learnerIcon from '../assets/learner.svg';
import IconLink from "./IconLink";
import CheckDeployButton from "./CheckDeployButton";

export default function LearnerChecklist() {
    const [isOpen, setIsOpen] = useState(true);
    return <FloatButton
        className={classes.floatButton}
        type="primary"
        icon={<CodeOutlined className={classes.floatIcon} />}
        onClick={() => setIsOpen(!isOpen)}
        description={
            <ChecklistContainer isOpen={isOpen}>
                <Checklist />
            </ChecklistContainer>
        }
    />
}

interface ChecklistContainerProps {
    isOpen: boolean
    children: ReactNode
}
function ChecklistContainer({ isOpen, children }: ChecklistContainerProps) {
    const { token } = theme.useToken();
    return <div onClick={(e) => { /* e.stopPropagation(); e.preventDefault(); */ }} className={`${classes.checklistContainer} ${isOpen ? classes.checklistContainerActive : ""}`} style={{ background: token.colorBgContainer }}>
        {children}
    </div>
}

interface Props { }

function Checklist({ }: Props) {
    const { token } = theme.useToken();
    const [checked, setChecked] = useState({
        playAround: false,
        seeCode: false,
        deploySite: false,
    })

    return <SocialNetworkPostBase
        Logo={<img width={32} style={{ padding: '8px' }} src={learnerIcon} alt="Icon" />}
        Header={<Typography.Title className={classes.checklistTitle}>Tech Checklist</Typography.Title>}
        Side={<div style={{ width: '48px', minWidth: '48px', padding: 'auto' }}>
            <div style={{ width: '1px', height: '100%', margin: 'auto', borderLeftColor: token.colorPrimary, borderLeftWidth: '1px', borderLeftStyle: 'solid' }} />
        </div>}
        Body={<Typography.Paragraph>Use this checklist as a guide through this site. </Typography.Paragraph>}
        Footer={<div className={classes.footerWrapper}>
            <ChecklistRow
                checked={checked.playAround}
                onChange={newValue => setChecked({ ...checked, playAround: newValue })}
                text="Play around the site as a user. Make your first post."
            />
            <ChecklistRow
                checked={checked.seeCode}
                onChange={newValue => setChecked({ ...checked, seeCode: newValue })}
                text="Take a look at the code on GitLab to see how this site is made."
                footer={
                    <div className={classes.checklistFooter} onClick={(e) => e.stopPropagation()}> <IconLink text={"Show me the code"} url={"#"} /></div>
                }
            />
            <ChecklistRow
                checked={checked.deploySite}
                onChange={newValue => setChecked({ ...checked, deploySite: newValue })}
                text="Use GitLab to deploy a change to this live site."
                footer={
                    <div className={classes.checklistFooter} onClick={(e) => e.stopPropagation()}>
                        <IconLink text={"See article"} url={"#"} />
                        <div className={classes.verifyDeployButton}>
                            <CheckDeployButton />
                        </div>
                    </div>
                }
            />
        </div>}
    />
}

interface ChecklistRowProps {
    checked: boolean
    onChange: (newValue: boolean) => void
    text: string
    footer?: ReactNode
}

function ChecklistRow({ checked, onChange, text, footer }: ChecklistRowProps) {
    return <div className={classes.checklistRow}>
        <Checkbox checked={checked} onChange={(e => onChange(e.target.checked))}>
            <Typography.Paragraph className={classes.checklistLabelText}>{text}</Typography.Paragraph>
        </Checkbox>
        {footer}
    </div>
}
