import { ReactNode, useState } from "react";
import { getAllPostsQuery } from "../social_network_post/queries/get_all_posts_query";
import AuthPromptModal from "./AuthPromptModal";
import { AuthPromptContext } from "./auth_prompt_context";
import { getUserIfLoggedInQuery } from "./queries/get_user_query";

interface Props {
    children: ReactNode
}

export default function AuthPromptProvider({ children }: Props) {
    const { refetch: getUserRefetch } = getUserIfLoggedInQuery();
    const [isOpenAuthPrompt, setIsOpenAuthPrompt] = useState(false);
    const { refetch: allPostsRefetch } = getAllPostsQuery();

    const handleLoginClose = () => {
        setIsOpenAuthPrompt(false);
        allPostsRefetch();
        getUserRefetch();
    }

    return <AuthPromptContext.Provider value={{
        promptLogin: () => setIsOpenAuthPrompt(true)
    }}>
        <AuthPromptModal
            isOpen={isOpenAuthPrompt}
            close={handleLoginClose}
        />
        {children}
    </AuthPromptContext.Provider>

}