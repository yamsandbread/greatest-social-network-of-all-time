import { GoogleOutlined } from '@ant-design/icons'
import { Button, Modal, theme, Typography } from 'antd'
import { useUseCasesProvider } from '../use_cases_provider/use_cases_provider_context'
import classes from './AuthPromptModal.module.css'

interface Props {
    isOpen: boolean
    close: () => void
}

export default function AuthPromptModal({ isOpen, close }: Props) {
    const { loginHandler } = useUseCasesProvider();
    const { token } = theme.useToken();

    const handleClick = async () => {
        await loginHandler.login();
        close();
    }
    return <Modal
        className={classes.modal}
        open={isOpen}
        footer={<></>}
        closable={false}
        onCancel={close}
        maskClosable={true}
    >
        <div className={classes.modal} style={{ background: token.colorBgContainer }}>
            <Typography.Title>Let's Set Things Up!</Typography.Title>
            <Button className={classes.button} icon={<GoogleOutlined />} onClick={handleClick}>Sign in with Google</Button>
        </div>
    </Modal>
}
