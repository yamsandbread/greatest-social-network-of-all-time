import { createContext, useContext } from "react";

export const AuthPromptContext = createContext<{
    promptLogin: () => void
}>({ promptLogin: () => undefined });

export function useAuthPrompt() {
    return useContext(AuthPromptContext);
}