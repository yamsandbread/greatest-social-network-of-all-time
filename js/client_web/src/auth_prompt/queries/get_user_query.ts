import { useQuery } from "react-query";
import { useUseCasesProvider } from "../../use_cases_provider/use_cases_provider_context";

export const getUserIfLoggedInQuery = () => {
    const { usersUseCases, loginHandler } = useUseCasesProvider();
    return useQuery('get-user', () => {
        if (loginHandler.isLoggedIn()) {
            return usersUseCases.getUser();
        } else {
            return undefined
        }
    }, {
        refetchOnMount: false,
        refetchOnReconnect: false,
        refetchOnWindowFocus: false,
    });
}