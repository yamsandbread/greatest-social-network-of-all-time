import classes from './App.module.css'
import Header from './header/Header'
import { ConfigProvider } from 'antd'
import { ThemeConfig } from 'antd/es/config-provider/context'
import UseCasesProvider from './use_cases_provider/UseCasesProvider'
import { QueryClientProvider } from 'react-query'
import AllPosts from './social_network_post/AllPosts'
import AuthPromptProvider from './auth_prompt/AuthPromptProvider'
import { queryClient } from './query_client'
import LearnerChecklist from './learner_checklist/LearnerChecklist'
import LearnerTheme from './LearnerTheme'

function App() {
  const theme = {
    token: {
      colorBgBase: "#3E2A35",
      colorBgContainer: "#FFFFFF",
      colorPrimary: "#0058C6",
      colorText: '#000000',
      fontSizeHeading1: 20,
    }
  } satisfies ThemeConfig;

  return (
    <QueryClientProvider client={queryClient}>
      <ConfigProvider theme={theme}>
        <div className={classes.base} style={{ background: theme.token.colorBgBase }}>
          <UseCasesProvider>
            <AuthPromptProvider>
              <LearnerTheme>
                <LearnerChecklist />
              </LearnerTheme>
              <Header />
              <div className={classes.allPostsWrapper}>
                <AllPosts />
              </div>
            </AuthPromptProvider>
          </UseCasesProvider>
        </div>
      </ConfigProvider>
    </QueryClientProvider>
  )
}

export default App
