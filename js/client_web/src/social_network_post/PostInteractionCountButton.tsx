import { Button, theme } from 'antd'
import classes from './PostInteractionCountButton.module.css'

interface Props {
    text: string
    count: number
    disabled?: boolean
    loading?: boolean
    title?: string
    onClick?: () => void
}

export default function PostInteractionCountButton({ text, count, disabled, loading, title, onClick }: Props) {
    const { token } = theme.useToken();

    return <div className={classes.base}>
        <div className={classes.countContainer} style={{ borderColor: token.colorPrimary }}>
            {count}
        </div>
        <Button
            type='primary'
            disabled={disabled}
            loading={loading}
            title={title}
            onClick={onClick}
            className={classes.button}
            style={{ borderTopLeftRadius: 0, borderBottomLeftRadius: 0 }}
        >
            {text}
        </Button>
    </div>
}