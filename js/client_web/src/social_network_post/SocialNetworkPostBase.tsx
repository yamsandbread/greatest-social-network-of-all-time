import { theme } from "antd";
import classes from './SocialNetworkPostBase.module.css'

interface Props {
    Logo: React.ReactNode
    Header: React.ReactNode
    Side: React.ReactNode
    Body: React.ReactNode
    Footer: React.ReactNode
}

export default function SocialNetworkPostBase(props: Props) {
    const { token } = theme.useToken();
    return <div className={classes.base} style={{ background: token.colorBgContainer }}>
        <div className={classes.headerRow}>
            {props.Logo}
            {props.Header}
        </div>
        <div className={classes.bodyRow}>
            {props.Side}
            {props.Body}
        </div>
        {props.Footer}
    </div>
}