import { Post } from "@social_network/social_network_posts/models/post/post";
import { InfiniteData, useMutation } from "react-query";
import { queryClient } from "../../query_client";
import { useUseCasesProvider } from "../../use_cases_provider/use_cases_provider_context";

export const interactWithPostMutation = () => {
    const postsUseCases = useUseCasesProvider().postsUseCases;
    type AllPostsResult = Awaited<ReturnType<typeof postsUseCases.getAll>>
    return useMutation(({ post, isPositiveInteraction }: { post: Post, isPositiveInteraction: boolean }) =>
        postsUseCases.interactWithPost(post, isPositiveInteraction), {
        onSuccess: (updatedPost: Post) => {
            queryClient.setQueryData('get-all-posts', (data: InfiniteData<AllPostsResult> | undefined) => {
                const newData = data!;

                for (let i = 0; i < newData.pages.length; i++) {
                    const page = newData.pages[i];
                    for (let j = 0; j < page.posts.length; j++) {
                        const post = page.posts[j];
                        if (post.userId === updatedPost.userId && post.createdAt === updatedPost.createdAt) {
                            page.posts[j] = updatedPost;
                            return newData;
                        }
                    }
                }
                return newData;
            })
        }
    });
}