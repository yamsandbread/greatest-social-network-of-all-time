import { useInfiniteQuery } from "react-query";
import { useUseCasesProvider } from "../../use_cases_provider/use_cases_provider_context";

export const getAllPostsQuery = () => {
    const pageSize = 10;
    const { postsUseCases } = useUseCasesProvider();
    return useInfiniteQuery('get-all-posts', ({ pageParam = "" }) => {
        return postsUseCases.getAll(pageParam, pageSize)
    }, {
        getNextPageParam: (lastPage, allPages) => {
            return lastPage.nextPaginationKey || undefined;

        },
        refetchOnMount: false,
        refetchOnReconnect: false,
        refetchOnWindowFocus: false,
    });
}