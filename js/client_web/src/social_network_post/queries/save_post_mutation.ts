import { useMutation } from "react-query";
import { useUseCasesProvider } from "../../use_cases_provider/use_cases_provider_context";

export const savePostMutation = () => {
    const postsUseCases = useUseCasesProvider().postsUseCases;
    return useMutation((text: string) => postsUseCases.savePost(text));
}