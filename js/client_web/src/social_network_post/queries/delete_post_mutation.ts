import { Post } from "@social_network/social_network_posts/models/post/post";
import { useMutation } from "react-query";
import { useUseCasesProvider } from "../../use_cases_provider/use_cases_provider_context";

export const deletePostMutation = () => {
    const postsUseCases = useUseCasesProvider().postsUseCases;
    return useMutation((post: Post) => postsUseCases.deletePost(post));
}