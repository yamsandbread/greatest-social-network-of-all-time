import { PlusOutlined } from "@ant-design/icons"
import { Button } from "antd"
import { useState } from "react"
import { useAuthPrompt } from "../auth_prompt/auth_prompt_context"
import { useUseCasesProvider } from "../use_cases_provider/use_cases_provider_context"
import classes from './AddNewPostButton.module.css'
import NewPostModal from "./NewPostModal"

interface Props {
}

export default function AddNewPostButton() {
    const { loginHandler } = useUseCasesProvider();
    const [isOpen, setIsOpen] = useState(false);
    const { promptLogin } = useAuthPrompt();

    const handleClick = () => {
        if (loginHandler.isLoggedIn()) {
            setIsOpen(true);
        } else {
            promptLogin();
        }
    }


    return <>
        <Button
            type='primary'
            size='large'
            className={classes.button}
            icon={<PlusOutlined />}
            onClick={handleClick}
        >Add Post</Button>
        <NewPostModal
            isOpen={isOpen}
            close={() => setIsOpen(false)}
        />
    </>
}