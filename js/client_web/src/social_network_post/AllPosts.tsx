import { ReloadOutlined } from "@ant-design/icons";
import { Button, Spin, theme } from "antd";
import { getAllPostsQuery } from "./queries/get_all_posts_query";
import SocialNetworkerPost from "./SocialNetworkerPost";
import classes from './AllPosts.module.css'
import { getUserIfLoggedInQuery } from "../auth_prompt/queries/get_user_query";

interface Props { }

export default function AllPosts({ }: Props) {
    const { token } = theme.useToken();
    const { data: currentUser } = getUserIfLoggedInQuery();
    const { data, isLoading, isError, fetchNextPage, hasNextPage, isFetching } = getAllPostsQuery();

    if (isLoading) {
        return <div className={classes.loaderWrapper} style={{ background: token.colorBgContainer }}>
            <Spin />
        </div>
    }

    if (isError || !data) {
        return <div className={classes.errorWrapper} style={{ background: token.colorBgContainer }}>
            Something went wrong. While this error is not intentional, this is a chance to open up the debug window and see if you can see what went wrong.
        </div>
    }

    const allPosts = data.pages.reduce((allPosts, currentPage) => allPosts.concat(currentPage.posts), [] as typeof data['pages'][0]['posts']);

    return <div className={classes.base}>
        <div className={classes.listWrapper}>
            {allPosts.map(post =>
                <div key={`${post.userId}|${post.createdAt}`} className={classes.postWrapper}>
                    <SocialNetworkerPost post={post} currentUser={currentUser} />
                </div>
            )}
        </div>
        <div className={classes.listEndWrapper}>
            {hasNextPage
                ? <Button onClick={() => fetchNextPage()} loading={isFetching} icon={<ReloadOutlined />}>Load More</Button>
                : <div className={classes.listDoneWrapper} style={{ background: token.colorBgContainer }}>You read all the posts!</div>
            }
        </div>
    </div>
}
