import SocialNetworkPostBase from "./SocialNetworkPostBase";
import socialNetworkerIcon from '../assets/social_networker.svg';
import { Button, Modal, Space, theme, Tooltip } from "antd";
import PostInteractionCountButton from "./PostInteractionCountButton";
import { Post } from "@social_network/social_network_posts/models/post/post";
import { User } from "@social_network/auth/models/user/user";
import { ReactNode } from "react";
import classes from './SocialNetworkerPost.module.css'
import { useAuthPrompt } from "../auth_prompt/auth_prompt_context";
import { DeleteOutlined } from "@ant-design/icons";
import { deletePostMutation } from "./queries/delete_post_mutation";
import { getAllPostsQuery } from "./queries/get_all_posts_query";
import { interactWithPostMutation } from "./queries/interact_with_post_mutation";

interface Props {
    post: Post
    currentUser?: User
}

export default function SocialNetworkerPost({ post, currentUser }: Props) {
    const { token } = theme.useToken();
    const { mutate } = deletePostMutation();
    const { refetch } = getAllPostsQuery();
    const { mutate: interactWithPostMutate, isLoading } = interactWithPostMutation();

    const onDeleteClick = () => {
        Modal.confirm({
            title: "Whaaaat?!?!",
            content: "You really want to erase this one after you put it out there?",
            cancelText: "Nope",
            okText: "Yes I do",
            onOk: () => {
                mutate(post, {
                    onSuccess: () => {
                        refetch();
                    }
                });
            }
        });
    }

    const onInteractionClick = () => {
        interactWithPostMutate({
            post,
            isPositiveInteraction: !post.hasInteractionByUser
        });
    }

    return <SocialNetworkPostBase
        Logo={<img width={40} src={socialNetworkerIcon} alt="Icon" />}
        Header={<div style={{ minHeight: '1px', width: '100%', borderTopColor: token.colorPrimary, borderTopWidth: '1px', borderTopStyle: 'solid' }} />}
        Side={<div style={{ width: '40px', minWidth: '40px', padding: 'auto' }}>
            <div style={{ width: '1px', height: '100%', margin: 'auto', borderLeftColor: token.colorPrimary, borderLeftWidth: '1px', borderLeftStyle: 'solid' }} />
        </div>}
        Body={<div style={{ width: '100%', paddingBottom: '4px', textAlign: 'start' }}>{post.text}</div>}
        Footer={<div style={{ paddingTop: '8px', paddingBottom: '8px', display: 'flex', justifyContent: 'end' }}>
            <SignInTooltip shouldPrompt={!currentUser}>
                <Space direction="horizontal">
                    {currentUser?.id === post.userId && <Button title="Delete post" onClick={onDeleteClick} icon={<DeleteOutlined />} />}
                    <PostInteractionCountButton
                        onClick={onInteractionClick}
                        disabled={!currentUser || isLoading}
                        text={post.hasInteractionByUser ? "Take it Back" : "Relatable"}
                        count={post.interactionCount}
                    />
                </Space>
            </SignInTooltip>
        </div>}
    />
}

interface TooltipProps {
    shouldPrompt: boolean
    children: ReactNode
}

export function SignInTooltip({ shouldPrompt, children }: TooltipProps) {
    const { promptLogin } = useAuthPrompt();
    if (!shouldPrompt) {
        return <>{children}</>;
    }

    return <Tooltip title={
        <div className={classes.signInTooltip}>
            <div>Sign in to interact with the posts.</div>
            <Button onClick={() => promptLogin()}>Sign in!</Button>
        </div>
    }>
        <span>
            {children}
        </span>
    </Tooltip>
}