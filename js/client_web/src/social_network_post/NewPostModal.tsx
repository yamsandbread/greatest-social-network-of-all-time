import SocialNetworkPostBase from "./SocialNetworkPostBase";
import socialNetworkerIcon from '../assets/social_networker.svg';
import { Button, Input, Modal, theme, Typography } from "antd";
import classes from './NewPostModal.module.css'
import { useState } from "react";
import { savePostMutation } from "./queries/save_post_mutation";

interface Props {
    isOpen: boolean
    close: () => void
}

export default function NewPostModal({ isOpen, close }: Props) {
    return <Modal
        open={isOpen}
        footer={<></>}
        closable={false}
        onCancel={close}
        maskClosable={true}
    >
        <NewPostForm close={close} />
    </Modal>
}

interface PostFormProps {
    close: () => void
}

function NewPostForm({ close }: PostFormProps) {
    const { token } = theme.useToken();
    const [inputText, setInputText] = useState("");
    const { isLoading, isError, data, error, mutate: savePostMutate } = savePostMutation();

    const fakeMaxCharacters = 20;
    const maxCharacters = 40;

    const handleClick = async () => {
        savePostMutate(inputText);
    }

    return <SocialNetworkPostBase
        Logo={<img width={40} src={socialNetworkerIcon} alt="Icon" />}
        Header={<Typography.Title className={classes.modalTitle}>Blurt it Out.</Typography.Title>}
        Side={<div style={{ width: '40px', minWidth: '40px', padding: 'auto' }}>
            <div style={{ width: '1px', height: '100%', margin: 'auto', borderLeftColor: token.colorPrimary, borderLeftWidth: '1px', borderLeftStyle: 'solid' }} />
        </div>}
        Body={<div className={classes.modalBody}>
            <Input.TextArea
                className={classes.input}
                value={inputText}
                onChange={(event) => setInputText(event.target.value)}
                placeholder="Share your deepest, silliest secrets."
                maxLength={maxCharacters}
                autoSize={true}
            />
            <div>{inputText.length}/{fakeMaxCharacters}</div>
        </div>}
        Footer={
            <div>
                <div className={classes.footer}>
                    <Button
                        className={`${classes.modalButton}`}
                        onClick={close}
                        disabled={isLoading}
                    >No... <br /> Back Out</Button>
                    <Button
                        className={`${classes.confirmButton} ${classes.modalButton}`}
                        type="primary"
                        onClick={handleClick}
                        loading={isLoading}
                    >Yes! <br /> Share This Secret</Button>
                </div>
                <div className={classes.hintText}>* You can always come back and delete your post, if courage is no longer on your side.</div>
            </div>
        }
    />
}