import { HttpRequester } from '@social_network/http_requester'
import { DeployVerificationRepoHttp } from './models/deploy_verification_repo/deploy_verification_repo_http';

export type LearnerUseCases = ReturnType<typeof getLearnerUseCases>;

export const getLearnerUseCases = ({ httpRequester }: { httpRequester: HttpRequester }) => {
    const deployVerificationRepo = DeployVerificationRepoHttp({ httpRequester })
    return {
        getPublicCi: async (fileName: string) => {
            return deployVerificationRepo.getPublicCi(fileName);
        }
    }
};