import { GetPublicCiRequest } from '@social_network/network_contracts'
import { HttpRequester } from '@social_network/http_requester'
import { DeployVerificationRepo } from "./deploy_verification_repo";

export const DeployVerificationRepoHttp = ({ httpRequester }: { httpRequester: HttpRequester }): DeployVerificationRepo => {
    return {
        getPublicCi: async (fileName: string) => {
            const requestBody: GetPublicCiRequest = {
                fileName
            };

            const response = await httpRequester.get<Record<string, unknown>>({
                path: '/public-ci',
                queryParams: requestBody
            });

            return response
        },
    }
}
