export interface DeployVerificationRepo {
    getPublicCi(fileName: string): Promise<Record<string, unknown>>
}
