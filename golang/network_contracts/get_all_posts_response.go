package networkcontracts

// This class is autogenerated using npm run generateClasses
type GetAllPostsResponse struct {
	Data []struct {
		UserId               string `json:"userId"`
		CreatedAt            string `json:"createdAt"`
		UserNickName         string `json:"userNickName"`
		Text                 string `json:"text"`
		InteractionCount     int64  `json:"interactionCount"`
		HasInteractionByUser bool   `json:"hasInteractionByUser"`
	} `json:"data"`
	NextPaginationKey string `json:"nextPaginationKey"`
}
