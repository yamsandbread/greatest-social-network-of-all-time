package main

import (
	"context"
	"encoding/json"

	"draw-on-animation/business-logic/users"
	networkcontracts "draw-on-animation/network-contracts"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func HandleRequest(ctx context.Context, event events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	usecases := users.NewUseCases()
	userId, ok := event.RequestContext.Authorizer["lambda"].(map[string]interface{})["userId"].(string)

	if !ok {
		return events.APIGatewayProxyResponse{
			StatusCode: 400,
		}, nil
	}

	result, err := usecases.InitializeUser(userId)

	stringBody, _ := json.Marshal(networkcontracts.InitializeUserResponse{
		Data: struct {
			Id       string "json:\"id\""
			NickName string "json:\"nickName\""
			IsNew    bool   "json:\"isNew\""
		}{
			Id:       result.Id,
			NickName: result.NickName,
			IsNew:    result.IsNew,
		},
	})
	return events.APIGatewayProxyResponse{
		Headers:    map[string]string{"Content-Type": "application/json"},
		StatusCode: 200,
		Body:       string(stringBody),
	}, err
}

func main() {
	lambda.Start(HandleRequest)
}
