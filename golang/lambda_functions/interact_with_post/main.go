package main

import (
	"context"
	"encoding/json"

	"draw-on-animation/business-logic/social_network_posts"
	networkcontracts "draw-on-animation/network-contracts"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func HandleRequest(ctx context.Context, event events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	usecases := social_network_posts.NewUseCases()
	userId, ok := event.RequestContext.Authorizer["lambda"].(map[string]interface{})["userId"].(string)

	if !ok {
		return events.APIGatewayProxyResponse{
			StatusCode: 400,
		}, nil
	}

	var requestBody networkcontracts.InteractWithPostRequest
	err := json.Unmarshal([]byte(event.Body), &requestBody)

	if err != nil {
		return events.APIGatewayProxyResponse{
			StatusCode: 400,
		}, nil
	}

	result, err := usecases.InteractWithPost(userId, requestBody.PostUserId, requestBody.PostCreatedAt, requestBody.IsPositiveInteraction)

	stringBody, _ := json.Marshal(networkcontracts.InteractWithPostResponse{
		Data: struct {
			UserId               string "json:\"userId\""
			CreatedAt            string "json:\"createdAt\""
			UserNickName         string "json:\"userNickName\""
			Text                 string "json:\"text\""
			InteractionCount     int64  "json:\"interactionCount\""
			HasInteractionByUser bool   "json:\"hasInteractionByUser\""
		}{
			UserId:               result.UserId,
			CreatedAt:            result.CreatedAt,
			UserNickName:         result.UserNickName,
			Text:                 result.Text,
			HasInteractionByUser: result.HasInteractionByUser,
			InteractionCount:     result.InteractionCount,
		},
	})
	return events.APIGatewayProxyResponse{
		Headers:    map[string]string{"Content-Type": "application/json"},
		StatusCode: 200,
		Body:       string(stringBody),
	}, err
}

func main() {
	lambda.Start(HandleRequest)
}
