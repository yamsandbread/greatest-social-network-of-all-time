package main

import (
	"context"

	"draw-on-animation/business-logic/auth"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func HandleRequest(ctx context.Context, event events.APIGatewayV2CustomAuthorizerV2Request) (events.APIGatewayV2CustomAuthorizerSimpleResponse, error) {
	usecases := auth.NewUseCases()
	result, err := usecases.GetUserLoginFromToken(event.IdentitySource[0])
	return events.APIGatewayV2CustomAuthorizerSimpleResponse{
		IsAuthorized: err == nil,
		Context: map[string]interface{}{
			"userId": result.UserId,
		},
	}, nil
}

func main() {
	lambda.Start(HandleRequest)
}
