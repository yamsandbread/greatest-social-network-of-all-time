package main

import (
	"context"
	"encoding/json"
	"strconv"

	"draw-on-animation/business-logic/social_network_posts"
	networkcontracts "draw-on-animation/network-contracts"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func HandleRequest(ctx context.Context, event events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	usecases := social_network_posts.NewUseCases()
	userId, _ := event.RequestContext.Authorizer["lambda"].(map[string]interface{})["userId"].(string)

	pageSize, err := strconv.Atoi(event.QueryStringParameters["pageSize"])
	requestBody := networkcontracts.GetAllPostsRequest{
		PaginationKey: event.QueryStringParameters["paginationKey"],
		PageSize:      int64(pageSize),
	}

	if err != nil {
		return events.APIGatewayProxyResponse{
			StatusCode: 400,
		}, err
	}

	result, nextPaginationKey, err := usecases.GetAllPosts(userId, requestBody.PaginationKey, requestBody.PageSize)

	type ResponsePost = struct {
		UserId               string "json:\"userId\""
		CreatedAt            string "json:\"createdAt\""
		UserNickName         string "json:\"userNickName\""
		Text                 string "json:\"text\""
		InteractionCount     int64  "json:\"interactionCount\""
		HasInteractionByUser bool   "json:\"hasInteractionByUser\""
	}
	responsePosts := make([]ResponsePost, len(result))
	for i := 0; i < len(result); i++ {
		responsePosts[i] = ResponsePost{
			UserId:               result[i].UserId,
			CreatedAt:            result[i].CreatedAt,
			UserNickName:         result[i].UserNickName,
			Text:                 result[i].Text,
			HasInteractionByUser: result[i].HasInteractionByUser,
			InteractionCount:     result[i].InteractionCount,
		}
	}

	stringBody, _ := json.Marshal(networkcontracts.GetAllPostsResponse{
		NextPaginationKey: nextPaginationKey,
		Data:              responsePosts,
	})
	return events.APIGatewayProxyResponse{
		Headers:    map[string]string{"Content-Type": "application/json"},
		StatusCode: 200,
		Body:       string(stringBody),
	}, err
}

func main() {
	lambda.Start(HandleRequest)
}
