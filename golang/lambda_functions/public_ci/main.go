package main

import (
	"context"
	"encoding/json"

	"draw-on-animation/business-logic/local_files"
	networkcontracts "draw-on-animation/network-contracts"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func HandleRequest(ctx context.Context, event events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	usecases := local_files.NewUseCases()
	requestBody := networkcontracts.GetPublicCiRequest{
		FileName: event.QueryStringParameters["fileName"],
	}

	result, err := usecases.GetLocalFile(requestBody.FileName)

	if err != nil {
		return events.APIGatewayProxyResponse{
			StatusCode: 400,
		}, err
	}

	stringBody, _ := json.Marshal(result)
	return events.APIGatewayProxyResponse{
		Headers:    map[string]string{"Content-Type": "application/json"},
		StatusCode: 200,
		Body:       string(stringBody),
	}, err
}

func main() {
	lambda.Start(HandleRequest)
}
