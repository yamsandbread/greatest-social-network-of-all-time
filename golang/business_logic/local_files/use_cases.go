package local_files

import (
	"draw-on-animation/business-logic/local_files/use_cases/get_local_file"

	"github.com/samber/do"
)

type UseCases struct {
	Injector *do.Injector
}

func NewUseCases() UseCases {
	return UseCases{
		Injector: getInjector(),
	}
}

func (self *UseCases) GetLocalFile(fileName string) (map[string]string, error) {
	return get_local_file.Invoke(self.Injector, fileName)
}
