package local_files

import (
	repo_use_cases "draw-on-animation/business-logic/repositories"
	"draw-on-animation/business-logic/repositories/models/local_files"

	"github.com/samber/do"
)

func getInjector() *do.Injector {

	injector := do.New()
	defer injector.Shutdown()

	do.Provide(injector, func(i *do.Injector) (local_files.LocalFilesRepo, error) {
		use_cases := repo_use_cases.NewUseCases()
		return use_cases.GetLocalFilesRepository(), nil
	})
	return injector
}
