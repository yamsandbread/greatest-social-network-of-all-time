package get_local_file

import (
	"draw-on-animation/business-logic/repositories/models/local_files"

	"github.com/samber/do"
)

func Invoke(injector *do.Injector, fileName string) (map[string]string, error) {

	repo := do.MustInvoke[local_files.LocalFilesRepo](injector)

	return repo.GetLocalJsonMap("golang/lambda_functions/public_ci/public_json/" + fileName + ".json")
}
