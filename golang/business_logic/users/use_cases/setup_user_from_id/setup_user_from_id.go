package setup_user_from_id

import (
	"draw-on-animation/business-logic/repositories/models/user_logins"
	"draw-on-animation/business-logic/repositories/models/users"
	"draw-on-animation/business-logic/users/models/user"
	"errors"

	"github.com/samber/do"
)

func Invoke(injector *do.Injector, id string) (user.User, error) {
	userRepo := do.MustInvoke[users.UsersRepo](injector)
	userLoginsRepo := do.MustInvoke[user_logins.UserLoginsRepo](injector)

	userEntity, found, err := userRepo.GetById(id)

	if err != nil {
		return user.User{}, err
	}

	if !found {
		userLoginEntity, found, err := userLoginsRepo.GetUserLoginByUserId(id)

		if err != nil {
			return user.User{}, err
		}
		if !found {
			return user.User{}, errors.New("Could not find user login")
		}

		userEntity = user.New(userLoginEntity.UserId)
		err = userRepo.Save(userEntity)

		if err != nil {
			return user.User{}, err
		}
	}

	return userEntity, nil
}
