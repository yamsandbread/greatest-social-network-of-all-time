package setup_user_from_id_test

import (
	"draw-on-animation/business-logic/auth/models/user_login"
	"draw-on-animation/business-logic/repositories/models/user_logins"
	users_repo "draw-on-animation/business-logic/repositories/models/users"
	"draw-on-animation/business-logic/users"
	"draw-on-animation/business-logic/users/models/user"
	"testing"

	"github.com/samber/do"
	"github.com/stretchr/testify/mock"
)

func getMockedUseCases() (*users_repo.UsersRepoMock, *user_logins.UserLoginsRepoMock, users.UseCases) {
	usecases := users.NewUseCases()
	mockUsersRepo := &users_repo.UsersRepoMock{}
	mockUserLoginRepo := &user_logins.UserLoginsRepoMock{}

	mockUsersRepo.On("GetById", "missingId").Return(user.User{}, false, nil)
	mockUsersRepo.On("GetById", "wrongId").Return(user.User{}, false, nil)
	mockUsersRepo.On("GetById", "validId").Return(user.User{
		Id: "SomeId",
	}, true, nil)

	mockUsersRepo.On("Save", mock.AnythingOfType("user.User")).Return(nil)

	mockUserLoginRepo.On("GetUserLoginByUserId", "validId").Return(user_login.UserLogin{
		UserId: "testUserId",
	}, true, nil)

	mockUserLoginRepo.On("GetUserLoginByUserId", "missingId").Return(user_login.UserLogin{
		UserId: "testMissingUserId",
	}, true, nil)

	mockUserLoginRepo.On("GetUserLoginByUserId", "wrongId").Return(user_login.UserLogin{}, false, nil)

	mockUserLoginRepo.On("SaveUserLogin", mock.AnythingOfType("user_login.UserLogin")).Return(nil)

	do.Override(usecases.Injector, func(injector *do.Injector) (users_repo.UsersRepo, error) {
		return mockUsersRepo, nil
	})

	do.Override(usecases.Injector, func(injector *do.Injector) (user_logins.UserLoginsRepo, error) {
		return mockUserLoginRepo, nil
	})
	return mockUsersRepo, mockUserLoginRepo, usecases
}

func TestReturnExistingUserIfExists(t *testing.T) {
	mockUsersRepo, mockUserLoginRepo, usecases := getMockedUseCases()
	_, err := usecases.InitializeUser("validId")
	if err != nil {
		t.Fatalf("Unexpected Error" + err.Error())
	}

	mockUsersRepo.AssertNumberOfCalls(t, "GetById", 1)
	mockUsersRepo.AssertNumberOfCalls(t, "Save", 0)
	mockUserLoginRepo.AssertNumberOfCalls(t, "GetUserLoginByUserId", 0)
}

func TestReturnNewUserIfNotExists(t *testing.T) {
	mockUsersRepo, mockUserLoginRepo, usecases := getMockedUseCases()
	_, err := usecases.InitializeUser("missingId")
	if err != nil {
		t.Fatalf("Unexpected Error" + err.Error())
	}

	mockUsersRepo.AssertNumberOfCalls(t, "GetById", 1)
	mockUsersRepo.AssertNumberOfCalls(t, "Save", 1)
	mockUserLoginRepo.AssertNumberOfCalls(t, "GetUserLoginByUserId", 1)
}

func TestReturnErrorIfNoUserLogin(t *testing.T) {
	mockUsersRepo, mockUserLoginRepo, usecases := getMockedUseCases()
	_, err := usecases.InitializeUser("wrongId")
	if err == nil {
		t.Fatalf("Expected an error but no error thrown")
	}

	mockUsersRepo.AssertNumberOfCalls(t, "GetById", 1)
	mockUsersRepo.AssertNumberOfCalls(t, "Save", 0)
	mockUserLoginRepo.AssertNumberOfCalls(t, "GetUserLoginByUserId", 1)
}
