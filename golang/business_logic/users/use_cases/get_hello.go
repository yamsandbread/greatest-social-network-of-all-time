package gethello

import (
	"github.com/samber/do"
)

func Invoke(injector *do.Injector) (string, error) {
	return "This is hello!!", nil
}
