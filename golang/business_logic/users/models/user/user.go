package user

import "time"

type User struct {
	Id        string
	NickName  string
	CreatedAt string
	IsNew     bool
}

func New(id string) User {
	return User{
		Id:        id,
		NickName:  "SomeDefaultName!",
		IsNew:     true,
		CreatedAt: time.Now().UTC().Format(time.RFC3339),
	}
}
