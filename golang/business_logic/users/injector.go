package users

import (
	repo_use_cases "draw-on-animation/business-logic/repositories"
	"draw-on-animation/business-logic/repositories/models/user_logins"
	"draw-on-animation/business-logic/repositories/models/users"

	"github.com/samber/do"
)

func getInjector() *do.Injector {

	injector := do.New()
	defer injector.Shutdown()

	do.Provide(injector, func(i *do.Injector) (users.UsersRepo, error) {
		use_cases := repo_use_cases.NewUseCases()
		return use_cases.GetUsersRepository(), nil
	})

	do.Provide(injector, func(i *do.Injector) (user_logins.UserLoginsRepo, error) {
		use_cases := repo_use_cases.NewUseCases()
		return use_cases.GetUserLoginsRepository(), nil
	})
	return injector
}
