package users

import (
	"draw-on-animation/business-logic/users/models/user"
	gethello "draw-on-animation/business-logic/users/use_cases"
	"draw-on-animation/business-logic/users/use_cases/setup_user_from_id"

	"github.com/samber/do"
)

type UseCases struct {
	Injector *do.Injector
}

func NewUseCases() UseCases {
	return UseCases{
		Injector: getInjector(),
	}
}

func (self *UseCases) InitializeUser(id string) (user.User, error) {
	return setup_user_from_id.Invoke(self.Injector, id)
}

func (self *UseCases) GetHello() (string, error) {
	return gethello.Invoke(self.Injector)
}
