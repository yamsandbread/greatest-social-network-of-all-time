package repositories

import (
	"draw-on-animation/business-logic/repositories/models/local_files"
	"draw-on-animation/business-logic/repositories/models/posts"
	"draw-on-animation/business-logic/repositories/models/user_logins"
	"draw-on-animation/business-logic/repositories/models/users"

	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/samber/do"
)

func getInjector() *do.Injector {

	injector := do.New()
	defer injector.Shutdown()

	session := GetSession()
	svc := dynamodb.New(session)

	do.Provide(injector, func(i *do.Injector) (user_logins.UserLoginsRepo, error) {
		return &user_logins.UserLoginsRepoDynamo{
			Client: svc,
		}, nil
	})

	do.Provide(injector, func(i *do.Injector) (users.UsersRepo, error) {
		return &users.UsersRepoDynamo{
			Client: svc,
		}, nil
	})

	do.Provide(injector, func(i *do.Injector) (posts.PostsRepo, error) {
		return &posts.PostsRepoDynamo{
			Client: svc,
		}, nil
	})

	do.Provide(injector, func(i *do.Injector) (local_files.LocalFilesRepo, error) {
		return &local_files.LocalFilesRepoFileSystem{}, nil
	})

	return injector
}
