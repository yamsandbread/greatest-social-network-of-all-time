package repositories

import "github.com/aws/aws-sdk-go/aws/session"

var (
	instance *session.Session
)

func GetSession() *session.Session {
	if instance == nil {
		instance = session.Must(session.NewSessionWithOptions(session.Options{
			SharedConfigState: session.SharedConfigEnable,
		}))
	}
	return instance
}
