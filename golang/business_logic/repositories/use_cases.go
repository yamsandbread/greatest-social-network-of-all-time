package repositories

import (
	"draw-on-animation/business-logic/repositories/models/local_files"
	"draw-on-animation/business-logic/repositories/models/posts"
	"draw-on-animation/business-logic/repositories/models/user_logins"
	"draw-on-animation/business-logic/repositories/models/users"

	"github.com/samber/do"
)

type UseCases struct {
	Injector *do.Injector
}

func NewUseCases() UseCases {
	return UseCases{
		Injector: getInjector(),
	}
}

func (self *UseCases) GetUserLoginsRepository() user_logins.UserLoginsRepo {
	return do.MustInvoke[user_logins.UserLoginsRepo](self.Injector)
}

func (self *UseCases) GetUsersRepository() users.UsersRepo {
	return do.MustInvoke[users.UsersRepo](self.Injector)
}

func (self *UseCases) GetPostsRepository() posts.PostsRepo {
	return do.MustInvoke[posts.PostsRepo](self.Injector)
}

func (self *UseCases) GetLocalFilesRepository() local_files.LocalFilesRepo {
	return do.MustInvoke[local_files.LocalFilesRepo](self.Injector)
}
