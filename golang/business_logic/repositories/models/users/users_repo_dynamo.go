package users

import (
	"draw-on-animation/business-logic/users/models/user"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

type UsersRepoDynamo struct {
	Client *dynamodb.DynamoDB
}

func (self *UsersRepoDynamo) GetById(id string) (user.User, bool, error) {
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"Id": {
				S: aws.String(id),
			},
		},
		TableName: aws.String("USERS"),
	}

	result, err := self.Client.GetItem(input)
	if err != nil {
		return user.User{}, false, err
	}

	if len(result.Item) == 0 {
		return user.User{}, false, nil
	}

	var item user.User
	err = dynamodbattribute.UnmarshalMap(result.Item, &item)
	if err != nil {
		return user.User{}, false, err
	}

	return item, true, nil
}

func (self *UsersRepoDynamo) Save(item user.User) error {
	dbPayload, err := dynamodbattribute.MarshalMap(item)
	if err != nil {
		return err
	}

	_, err = self.Client.PutItem(&dynamodb.PutItemInput{
		TableName: aws.String("USERS"),
		Item:      dbPayload,
	})
	return err
}
