package users

import (
	"draw-on-animation/business-logic/users/models/user"
)

type UsersRepo interface {
	GetById(id string) (user.User, bool, error)
	Save(item user.User) error
}
