package users

import (
	"draw-on-animation/business-logic/users/models/user"

	"github.com/stretchr/testify/mock"
)

type UsersRepoMock struct {
	mock.Mock
}

func (self *UsersRepoMock) GetById(id string) (user.User, bool, error) {
	args := self.Called(id)
	return args.Get(0).(user.User), args.Bool(1), args.Error(2)
}

func (self *UsersRepoMock) Save(user user.User) error {
	args := self.Called(user)
	return args.Error(0)
}
