package user_logins

import (
	"draw-on-animation/business-logic/auth/models/user_login"
)

type UserLoginsRepo interface {
	GetUserLogin(providerId, providerName string) (user_login.UserLogin, bool, error)
	GetUserLoginByUserId(userId string) (user_login.UserLogin, bool, error)
	SaveUserLogin(userLogin user_login.UserLogin) error
}
