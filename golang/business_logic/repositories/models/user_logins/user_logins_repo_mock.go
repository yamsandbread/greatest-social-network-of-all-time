package user_logins

import (
	"draw-on-animation/business-logic/auth/models/user_login"

	"github.com/stretchr/testify/mock"
)

type UserLoginsRepoMock struct {
	mock.Mock
}

func (self *UserLoginsRepoMock) GetUserLogin(providerId, providerName string) (user_login.UserLogin, bool, error) {
	args := self.Called(providerId, providerName)
	return args.Get(0).(user_login.UserLogin), args.Bool(1), args.Error(2)
}

func (self *UserLoginsRepoMock) GetUserLoginByUserId(userId string) (user_login.UserLogin, bool, error) {
	args := self.Called(userId)
	return args.Get(0).(user_login.UserLogin), args.Bool(1), args.Error(2)
}

func (self *UserLoginsRepoMock) SaveUserLogin(userLogin user_login.UserLogin) error {
	args := self.Called(userLogin)
	return args.Error(0)
}
