package user_logins

import (
	"draw-on-animation/business-logic/auth/models/user_login"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

type UserLoginsRepoDynamo struct {
	Client *dynamodb.DynamoDB
}

func (self *UserLoginsRepoDynamo) GetUserLogin(providerUserId, providerName string) (user_login.UserLogin, bool, error) {
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"ProviderUserId": {
				S: aws.String(providerUserId),
			},
			"ProviderName": {
				S: aws.String(providerName),
			},
		},
		TableName: aws.String("USER_LOGINS"),
	}

	result, err := self.Client.GetItem(input)
	if err != nil {
		return user_login.UserLogin{}, false, err
	}

	if len(result.Item) == 0 {
		return user_login.UserLogin{}, false, nil
	}

	var userLogin user_login.UserLogin
	err = dynamodbattribute.UnmarshalMap(result.Item, &userLogin)
	if err != nil {
		return user_login.UserLogin{}, false, err
	}

	return userLogin, true, nil
}

func (self *UserLoginsRepoDynamo) GetUserLoginByUserId(userId string) (user_login.UserLogin, bool, error) {
	condition := expression.Key("UserId").Equal(expression.Value(userId))
	expr, err := expression.NewBuilder().WithKeyCondition(condition).Build()
	if err != nil {
		return user_login.UserLogin{}, false, err
	}
	input := &dynamodb.QueryInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String("USER_LOGINS"),
		IndexName:                 aws.String("UserIdIndex"),
	}

	result, err := self.Client.Query(input)
	if err != nil {
		return user_login.UserLogin{}, false, err
	}

	if len(result.Items) == 0 {
		return user_login.UserLogin{}, false, nil
	}

	var userLogin user_login.UserLogin
	err = dynamodbattribute.UnmarshalMap(result.Items[0], &userLogin)
	if err != nil {
		return user_login.UserLogin{}, false, err
	}

	return userLogin, true, nil
}

func (self *UserLoginsRepoDynamo) SaveUserLogin(userLogin user_login.UserLogin) error {
	dbPayload, err := dynamodbattribute.MarshalMap(userLogin)
	if err != nil {
		return err
	}

	_, err = self.Client.PutItem(&dynamodb.PutItemInput{
		TableName: aws.String("USER_LOGINS"),
		Item:      dbPayload,
	})
	return err
}
