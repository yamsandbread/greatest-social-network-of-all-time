package posts

import (
	"draw-on-animation/business-logic/social_network_posts/models/post"

	"github.com/stretchr/testify/mock"
)

type PostsRepoMock struct {
	mock.Mock
}

func (self *PostsRepoMock) GetAll(paginationKey string, pageSize int64) ([]post.Post, string, error) {
	args := self.Called(paginationKey, pageSize)
	return args.Get(0).([]post.Post), args.String(1), args.Error(2)
}

func (self *PostsRepoMock) Save(item post.Post) error {
	args := self.Called(item)
	return args.Error(0)
}

func (self *PostsRepoMock) Delete(item post.Post) error {
	args := self.Called(item)
	return args.Error(0)
}

func (self *PostsRepoMock) Interact(userId string, item post.Post, isPositiveInteracation bool) (post.Post, bool, error) {
	args := self.Called(userId, item, isPositiveInteracation)
	return args.Get(0).(post.Post), args.Bool(1), args.Error(2)
}
