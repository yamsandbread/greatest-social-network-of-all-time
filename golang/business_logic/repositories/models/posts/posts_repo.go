package posts

import (
	"draw-on-animation/business-logic/social_network_posts/models/post"
)

type PostsRepo interface {
	Interact(userId string, item post.Post, isPositiveInteracation bool) (post.Post, bool, error)
	GetAll(paginationKey string, pageSize int64) ([]post.Post, string, error)
	Save(item post.Post) error
	Delete(item post.Post) error
}
