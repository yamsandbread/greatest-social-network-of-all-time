package posts

import (
	"draw-on-animation/business-logic/social_network_posts/models/post"
	"encoding/base64"
	"encoding/json"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

type PostsRepoDynamo struct {
	Client *dynamodb.DynamoDB
}

func getFirstTimePositiveInteractionInput(userId string, item post.Post) dynamodb.UpdateItemInput {
	return dynamodb.UpdateItemInput{
		TableName: aws.String("POSTS"),
		Key: map[string]*dynamodb.AttributeValue{
			"UserId": {
				S: aws.String(item.UserId),
			},
			"CreatedAt": {
				S: aws.String(item.CreatedAt),
			},
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":val": {M: map[string]*dynamodb.AttributeValue{
				userId: {BOOL: aws.Bool(true)},
			}},
		},
		UpdateExpression: aws.String("SET Interactions = :val"),
		ReturnValues:     aws.String("ALL_NEW"),
	}
}

func getPositiveInteractionInput(userId string, item post.Post) dynamodb.UpdateItemInput {
	return dynamodb.UpdateItemInput{
		TableName: aws.String("POSTS"),
		Key: map[string]*dynamodb.AttributeValue{
			"UserId": {
				S: aws.String(item.UserId),
			},
			"CreatedAt": {
				S: aws.String(item.CreatedAt),
			},
		},
		ExpressionAttributeNames: aws.StringMap(map[string]string{
			"#userId": userId,
		}),
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":val": {BOOL: aws.Bool(true)},
		},
		UpdateExpression: aws.String("SET Interactions.#userId = :val"),
		ReturnValues:     aws.String("ALL_NEW"),
	}
}

func getNegativeInteractionInput(userId string, item post.Post) dynamodb.UpdateItemInput {
	return dynamodb.UpdateItemInput{
		TableName: aws.String("POSTS"),
		Key: map[string]*dynamodb.AttributeValue{
			"UserId": {
				S: aws.String(item.UserId),
			},
			"CreatedAt": {
				S: aws.String(item.CreatedAt),
			},
		},
		ExpressionAttributeNames: aws.StringMap(map[string]string{
			"#userId": userId,
		}),
		UpdateExpression: aws.String("REMOVE Interactions.#userId"),
		ReturnValues:     aws.String("ALL_NEW"),
	}
}

func (self *PostsRepoDynamo) Interact(userId string, item post.Post, isPositiveInteracation bool) (post.Post, bool, error) {
	var input dynamodb.UpdateItemInput

	if isPositiveInteracation {
		input = getPositiveInteractionInput(userId, item)
	} else {
		input = getNegativeInteractionInput(userId, item)
	}

	result, err := self.Client.UpdateItem(&input)
	if err != nil {
		if awsErr, ok := err.(awserr.Error); ok && isPositiveInteracation {
			if awsErr.Code() == "ValidationException" {
				input = getFirstTimePositiveInteractionInput(userId, item)
				result, err = self.Client.UpdateItem(&input)
				if err != nil {
					return post.Post{}, false, err
				}
			} else {
				return post.Post{}, false, err
			}
		} else {
			return post.Post{}, false, err
		}
	}

	if len(result.Attributes) == 0 {
		return post.Post{}, false, nil
	}

	var updatedItem post.Post
	err = dynamodbattribute.UnmarshalMap(result.Attributes, &updatedItem)
	if err != nil {
		return post.Post{}, false, err
	}

	return updatedItem, true, nil
}

func (self *PostsRepoDynamo) GetAll(paginationKey string, pageSize int64) ([]post.Post, string, error) {
	condition := expression.Key("Status").Equal(expression.Value("ok"))
	expr, err := expression.NewBuilder().WithKeyCondition(condition).Build()

	input := &dynamodb.QueryInput{
		TableName:                 aws.String("POSTS"),
		IndexName:                 aws.String("StatusIndex"),
		Limit:                     aws.Int64(pageSize),
		ScanIndexForward:          aws.Bool(false),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		KeyConditionExpression:    expr.KeyCondition(),
	}
	if paginationKey != "" {
		decodedPaginationKey, err := base64.StdEncoding.DecodeString(paginationKey)
		if err != nil {
			return []post.Post{}, "", err
		}

		var lastEvaluatedKey map[string]*dynamodb.AttributeValue
		err = json.Unmarshal(decodedPaginationKey, &lastEvaluatedKey)
		if err != nil {
			return []post.Post{}, "", err
		}
		input.ExclusiveStartKey = lastEvaluatedKey
	}

	result, err := self.Client.Query(input)
	if err != nil {
		return []post.Post{}, "", err
	}

	var items []post.Post
	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &items)
	if err != nil {
		return []post.Post{}, "", err
	}
	lastEvaluatedKey := ""
	if result.LastEvaluatedKey != nil {
		lastEvaluatedKeyMarshalled, err := json.Marshal(result.LastEvaluatedKey)
		if err != nil {
			return []post.Post{}, "", err
		}
		lastEvaluatedKey = base64.StdEncoding.EncodeToString(lastEvaluatedKeyMarshalled)
	}
	return items, lastEvaluatedKey, nil
}

func (self *PostsRepoDynamo) Save(item post.Post) error {
	dbPayload, err := dynamodbattribute.MarshalMap(item)
	if err != nil {
		return err
	}

	dbPayload["Status"] = &dynamodb.AttributeValue{S: aws.String("ok")}

	_, err = self.Client.PutItem(&dynamodb.PutItemInput{
		TableName: aws.String("POSTS"),
		Item:      dbPayload,
	})
	return err
}

func (self *PostsRepoDynamo) Delete(item post.Post) error {
	input := &dynamodb.DeleteItemInput{
		TableName: aws.String("POSTS"),
		Key: map[string]*dynamodb.AttributeValue{
			"UserId": {
				S: aws.String(item.UserId),
			},
			"CreatedAt": {
				S: aws.String(item.CreatedAt),
			},
		},
	}

	_, err := self.Client.DeleteItem(input)
	return err
}
