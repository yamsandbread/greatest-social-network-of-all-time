package local_files

type LocalFilesRepo interface {
	GetLocalJsonMap(fileName string) (map[string]string, error)
}
