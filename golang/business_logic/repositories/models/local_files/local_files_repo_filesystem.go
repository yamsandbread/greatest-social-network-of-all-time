package local_files

import (
	"encoding/json"
	"os"
)

type LocalFilesRepoFileSystem struct{}

func (self *LocalFilesRepoFileSystem) GetLocalJsonMap(fileName string) (map[string]string, error) {

	fileContent, err := os.ReadFile(fileName)

	if err != nil {
		return nil, err
	}

	var jsonContent map[string]string
	err = json.Unmarshal(fileContent, &jsonContent)
	if err != nil {
		return nil, err
	}

	return jsonContent, nil
}
