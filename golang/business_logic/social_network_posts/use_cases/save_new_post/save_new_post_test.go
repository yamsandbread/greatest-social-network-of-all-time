package save_new_post_test

import (
	posts_repo "draw-on-animation/business-logic/repositories/models/posts"
	users_repo "draw-on-animation/business-logic/repositories/models/users"
	"draw-on-animation/business-logic/social_network_posts"
	"draw-on-animation/business-logic/social_network_posts/models/post"
	"draw-on-animation/business-logic/users/models/user"
	"testing"

	"github.com/samber/do"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func getMockedUseCases() (*users_repo.UsersRepoMock, *posts_repo.PostsRepoMock, social_network_posts.UseCases) {
	usecases := social_network_posts.NewUseCases()
	mockUsersRepo := &users_repo.UsersRepoMock{}
	mockPostsRepo := &posts_repo.PostsRepoMock{}

	mockUsersRepo.On("GetById", "testId").Return(user.User{
		Id:       "SomeId",
		NickName: "SomeNickName",
	}, true, nil)

	mockPostsRepo.On("Save", mock.AnythingOfType("post.Post")).Return(nil)

	do.Override(usecases.Injector, func(injector *do.Injector) (users_repo.UsersRepo, error) {
		return mockUsersRepo, nil
	})

	do.Override(usecases.Injector, func(injector *do.Injector) (posts_repo.PostsRepo, error) {
		return mockPostsRepo, nil
	})
	return mockUsersRepo, mockPostsRepo, usecases
}

func TestSavesPostWithCorrectUserInfo(t *testing.T) {
	mockUsersRepo, mockPostsRepo, usecases := getMockedUseCases()
	var newPost post.Post
	newPost, err := usecases.SavePost("testId", "This is my text!")
	if err != nil {
		t.Fatalf("Unexpected Error" + err.Error())
	}

	mockUsersRepo.AssertNumberOfCalls(t, "GetById", 1)
	mockPostsRepo.AssertNumberOfCalls(t, "Save", 1)
	assert.Equal(t, newPost.UserId, "SomeId", "UserId should be attached to the post")
	assert.Equal(t, newPost.UserNickName, "SomeNickName", "UserNickName should be attached to the post")
}

func TestErrorWhenPostTextIsTooLong(t *testing.T) {
	mockUsersRepo, mockPostsRepo, usecases := getMockedUseCases()
	_, err := usecases.SavePost("testId", "Longer than 40 chars! Longer than 40 chars! Longer than 40 chars!")
	if err == nil {
		t.Fatalf("Expected an error but no error thrown")
	}
	mockUsersRepo.AssertNumberOfCalls(t, "GetById", 0)
	mockPostsRepo.AssertNumberOfCalls(t, "Save", 0)
}

func TestErrorWhenPostTextIsTooShort(t *testing.T) {
	mockUsersRepo, mockPostsRepo, usecases := getMockedUseCases()
	_, err := usecases.SavePost("testId", "")
	if err == nil {
		t.Fatalf("Expected an error but no error thrown")
	}
	mockUsersRepo.AssertNumberOfCalls(t, "GetById", 0)
	mockPostsRepo.AssertNumberOfCalls(t, "Save", 0)
}
