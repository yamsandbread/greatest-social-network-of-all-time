package save_new_post

import (
	"draw-on-animation/business-logic/repositories/models/posts"
	"draw-on-animation/business-logic/repositories/models/users"
	"draw-on-animation/business-logic/social_network_posts/models/post"

	"errors"

	"github.com/samber/do"
)

func Invoke(injector *do.Injector, userId, text string) (post.Post, error) {
	if len(text) > 40 {
		return post.Post{}, errors.New("Text too long")
	}
	if len(text) <= 0 {
		return post.Post{}, errors.New("You need to write something... anything?")
	}

	postsRepo := do.MustInvoke[posts.PostsRepo](injector)
	userRepo := do.MustInvoke[users.UsersRepo](injector)

	userEntity, found, err := userRepo.GetById(userId)

	if err != nil {
		return post.Post{}, err
	}

	if !found {
		return post.Post{}, errors.New("Invalid user")
	}

	newPost := post.New(userEntity.Id, userEntity.NickName, text)
	err = postsRepo.Save(newPost)
	if err != nil {
		return post.Post{}, err
	}

	return newPost, nil
}
