package interact_with_post

import (
	"draw-on-animation/business-logic/repositories/models/posts"
	"draw-on-animation/business-logic/social_network_posts/models/post"

	"errors"

	"github.com/samber/do"
)

func Invoke(injector *do.Injector, userId, postUserId, postCreatedAt string, isPositiveInteraction bool) (post.UserPost, error) {
	postsRepo := do.MustInvoke[posts.PostsRepo](injector)
	updatedPost, found, err := postsRepo.Interact(userId, post.Post{
		UserId:    postUserId,
		CreatedAt: postCreatedAt,
	}, isPositiveInteraction)
	if err != nil {
		return post.UserPost{}, err
	}
	if !found {
		return post.UserPost{}, errors.New("Invalid post")
	}

	return post.FromPost(updatedPost, userId), nil
}
