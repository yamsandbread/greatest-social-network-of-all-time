package get_all_posts_test

import (
	posts_repo "draw-on-animation/business-logic/repositories/models/posts"
	users_repo "draw-on-animation/business-logic/repositories/models/users"
	"draw-on-animation/business-logic/social_network_posts"
	"draw-on-animation/business-logic/social_network_posts/models/post"
	"testing"

	"github.com/samber/do"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func getMockedUseCases() (*users_repo.UsersRepoMock, *posts_repo.PostsRepoMock, social_network_posts.UseCases) {
	usecases := social_network_posts.NewUseCases()
	mockUsersRepo := &users_repo.UsersRepoMock{}
	mockPostsRepo := &posts_repo.PostsRepoMock{}

	mockPostsRepo.On("GetAll", "soloPages", mock.Anything).Return([]post.Post{
		{
			Interactions: map[string]bool{
				"soloInteractedUserId": true,
			},
		},
	}, "", nil)

	mockPostsRepo.On("GetAll", "pages", mock.Anything).Return([]post.Post{
		{
			Interactions: map[string]bool{
				"interactedUserId": true,
				"temp1":            true,
				"temp2":            true,
				"temp3":            true,
				"temp4":            true,
				"temp5":            true,
			},
		},
	}, "", nil)

	mockPostsRepo.On("GetAll", "pages", mock.Anything).Return([]post.Post{{
		Interactions: map[string]bool{
			"temp1": true,
			"temp2": true,
			"temp3": true,
			"temp4": true,
			"temp5": true,
		},
	}}, "", nil)

	do.Override(usecases.Injector, func(injector *do.Injector) (users_repo.UsersRepo, error) {
		return mockUsersRepo, nil
	})

	do.Override(usecases.Injector, func(injector *do.Injector) (posts_repo.PostsRepo, error) {
		return mockPostsRepo, nil
	})
	return mockUsersRepo, mockPostsRepo, usecases
}

func TestHasInteractionTrueSolo(t *testing.T) {
	_, mockPostsRepo, usecases := getMockedUseCases()
	posts, _, err := usecases.GetAllPosts("soloInteractedUserId", "soloPages", 1)
	if err != nil {
		t.Fatalf("Unexpected Error" + err.Error())
	}

	mockPostsRepo.AssertNumberOfCalls(t, "GetAll", 1)
	assert.Equal(t, posts[0].HasInteractionByUser, true)
}

func TestHasInteractionFalseSolo(t *testing.T) {
	_, mockPostsRepo, usecases := getMockedUseCases()
	posts, _, err := usecases.GetAllPosts("soloNotInteractedUserId", "soloPages", 1)
	if err != nil {
		t.Fatalf("Unexpected Error" + err.Error())
	}

	mockPostsRepo.AssertNumberOfCalls(t, "GetAll", 1)
	assert.Equal(t, posts[0].HasInteractionByUser, false)
}

func TestInteractionCountIncludingUser(t *testing.T) {
	_, mockPostsRepo, usecases := getMockedUseCases()
	posts, _, err := usecases.GetAllPosts("interactedUserId", "pages", 1)
	if err != nil {
		t.Fatalf("Unexpected Error" + err.Error())
	}

	mockPostsRepo.AssertNumberOfCalls(t, "GetAll", 1)
	assert.Equal(t, posts[0].InteractionCount, int64(6))
}

func TestInteractionCountExcludingUser(t *testing.T) {
	_, mockPostsRepo, usecases := getMockedUseCases()
	posts, _, err := usecases.GetAllPosts("notInteractedUserId", "pages", 1)
	if err != nil {
		t.Fatalf("Unexpected Error" + err.Error())
	}

	mockPostsRepo.AssertNumberOfCalls(t, "GetAll", 1)
	assert.Equal(t, posts[0].InteractionCount, int64(6))
}

func TestNoErrorWhenNoUserId(t *testing.T) {
	_, mockPostsRepo, usecases := getMockedUseCases()
	_, _, err := usecases.GetAllPosts("", "pages", 1)
	if err != nil {
		t.Fatalf("Unexpected Error" + err.Error())
	}

	mockPostsRepo.AssertNumberOfCalls(t, "GetAll", 1)
}
