package get_all_posts

import (
	"draw-on-animation/business-logic/repositories/models/posts"
	"draw-on-animation/business-logic/social_network_posts/models/post"

	"github.com/samber/do"
)

func Invoke(injector *do.Injector, userId string, paginationKey string, pageSize int64) ([]post.UserPost, string, error) {
	postsRepo := do.MustInvoke[posts.PostsRepo](injector)

	posts, nextPaginationKey, err := postsRepo.GetAll(paginationKey, pageSize)
	if err != nil {
		return []post.UserPost{}, "", err
	}

	userPosts := make([]post.UserPost, len(posts))
	for i := 0; i < len(posts); i++ {
		userPosts[i] = post.FromPost(posts[i], userId)
	}

	return userPosts, nextPaginationKey, nil
}
