package delete_post

import (
	"draw-on-animation/business-logic/repositories/models/posts"
	"draw-on-animation/business-logic/social_network_posts/models/post"

	"github.com/samber/do"
)

func Invoke(injector *do.Injector, userId string, createdAt string) error {

	postsRepo := do.MustInvoke[posts.PostsRepo](injector)

	return postsRepo.Delete(post.Post{
		UserId:    userId,
		CreatedAt: createdAt,
	})
}
