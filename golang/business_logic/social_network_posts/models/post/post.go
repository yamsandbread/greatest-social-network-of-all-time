package post

import "time"

type Post struct {
	UserId       string
	CreatedAt    string
	UserNickName string
	Text         string
	Interactions map[string]bool
}

func New(userId, userNickName, text string) Post {
	return Post{
		UserId:       userId,
		CreatedAt:    time.Now().UTC().Format(time.RFC3339),
		UserNickName: userNickName,
		Text:         text,
		Interactions: map[string]bool{},
	}
}
