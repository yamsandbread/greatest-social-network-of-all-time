package post

type UserPost struct {
	UserId               string
	CreatedAt            string
	UserNickName         string
	Text                 string
	InteractionCount     int64
	HasInteractionByUser bool
}

func FromPost(post Post, userId string) UserPost {
	hasInteractionByUser := false
	if userId != "" {
		hasInteractionByUser = post.Interactions[userId] == true
	}

	interactionCount := len(post.Interactions)
	return UserPost{
		UserId:               post.UserId,
		CreatedAt:            post.CreatedAt,
		UserNickName:         post.UserNickName,
		Text:                 post.Text,
		InteractionCount:     int64(interactionCount),
		HasInteractionByUser: hasInteractionByUser,
	}
}
