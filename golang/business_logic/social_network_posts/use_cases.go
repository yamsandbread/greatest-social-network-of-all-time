package social_network_posts

import (
	"draw-on-animation/business-logic/social_network_posts/models/post"
	"draw-on-animation/business-logic/social_network_posts/use_cases/delete_post"
	"draw-on-animation/business-logic/social_network_posts/use_cases/get_all_posts"
	"draw-on-animation/business-logic/social_network_posts/use_cases/interact_with_post"
	"draw-on-animation/business-logic/social_network_posts/use_cases/save_new_post"

	"github.com/samber/do"
)

type UseCases struct {
	Injector *do.Injector
}

func NewUseCases() UseCases {
	return UseCases{
		Injector: getInjector(),
	}
}

func (self *UseCases) GetAllPosts(userId string, paginationKey string, pageSize int64) ([]post.UserPost, string, error) {
	return get_all_posts.Invoke(self.Injector, userId, paginationKey, pageSize)
}

func (self *UseCases) SavePost(userId, text string) (post.Post, error) {
	return save_new_post.Invoke(self.Injector, userId, text)
}

func (self *UseCases) DeletePost(userId, createdAt string) error {
	return delete_post.Invoke(self.Injector, userId, createdAt)
}

func (self *UseCases) InteractWithPost(userId, postUserId, postCreatedAt string, isPositiveInteraction bool) (post.UserPost, error) {
	return interact_with_post.Invoke(self.Injector, userId, postUserId, postCreatedAt, isPositiveInteraction)
}
