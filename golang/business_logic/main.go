package main

import (
	networkcontracts "draw-on-animation/network-contracts"
	"log"
)

func main() {
	test := networkcontracts.Test{
		StringProp: "ok",
		BoolProp:   true,
		IntProp:    2,
	}
	log.Println(test)
}
