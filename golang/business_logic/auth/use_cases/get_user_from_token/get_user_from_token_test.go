package get_user_from_token_test

import (
	"draw-on-animation/business-logic/auth"
	"draw-on-animation/business-logic/auth/models/auth_token_verification_result"
	"draw-on-animation/business-logic/auth/models/token_verifier"
	"draw-on-animation/business-logic/auth/models/user_login"
	"draw-on-animation/business-logic/repositories/models/user_logins"
	"errors"
	"testing"

	"github.com/samber/do"
	"github.com/stretchr/testify/mock"
)

func getMockedUseCases() (*token_verifier.TokenVerifierMock, *user_logins.UserLoginsRepoMock, auth.UseCases) {
	usecases := auth.NewUseCases()
	mockVerifier := &token_verifier.TokenVerifierMock{}
	mockUserLoginRepo := &user_logins.UserLoginsRepoMock{}

	mockVerifier.On("Verify", "token").Return(auth_token_verification_result.AuthTokenVerificationResult{
		Subject:        "testSubject",
		SignInProvider: "testProvider",
	}, nil)

	mockVerifier.On("Verify", "new_token").Return(auth_token_verification_result.AuthTokenVerificationResult{
		Subject:        "testSubjectNew",
		SignInProvider: "testProviderNew",
	}, nil)

	mockVerifier.On("Verify", "fail_token").Return(auth_token_verification_result.AuthTokenVerificationResult{}, errors.New("Error"))

	mockUserLoginRepo.On("GetUserLogin", "testSubject", "testProvider").Return(user_login.UserLogin{
		UserId: "testUserId",
	}, true, nil)

	mockUserLoginRepo.On("GetUserLogin", "testSubjectNew", "testProviderNew").Return(user_login.UserLogin{}, false, nil)

	mockUserLoginRepo.On("SaveUserLogin", mock.AnythingOfType("user_login.UserLogin")).Return(nil)

	do.Override(usecases.Injector, func(injector *do.Injector) (token_verifier.TokenVerifier, error) {
		return mockVerifier, nil
	})

	do.Override(usecases.Injector, func(injector *do.Injector) (user_logins.UserLoginsRepo, error) {
		return mockUserLoginRepo, nil
	})
	return mockVerifier, mockUserLoginRepo, usecases
}

func TestReturnExistingUserAfterSuccessfulVerification(t *testing.T) {
	mockVerifier, mockUserLoginRepo, usecases := getMockedUseCases()
	_, err := usecases.GetUserLoginFromToken("token")
	if err != nil {
		t.Fatalf("Unexpected Error" + err.Error())
	}

	mockVerifier.AssertNumberOfCalls(t, "Verify", 1)
	mockUserLoginRepo.AssertNumberOfCalls(t, "GetUserLogin", 1)
	mockUserLoginRepo.AssertNumberOfCalls(t, "SaveUserLogin", 0)
}

func TestBearerStringShouldNotHaveAnImpact(t *testing.T) {
	mockVerifier, mockUserLoginRepo, usecases := getMockedUseCases()
	_, err := usecases.GetUserLoginFromToken("Bearer token")
	if err != nil {
		t.Fatalf("Unexpected Error" + err.Error())
	}

	mockVerifier.AssertNumberOfCalls(t, "Verify", 1)
	mockUserLoginRepo.AssertNumberOfCalls(t, "GetUserLogin", 1)
	mockUserLoginRepo.AssertNumberOfCalls(t, "SaveUserLogin", 0)
}

func TestReturnNewUserAfterSuccessfulVerification(t *testing.T) {
	mockVerifier, mockUserLoginRepo, usecases := getMockedUseCases()
	_, err := usecases.GetUserLoginFromToken("new_token")
	if err != nil {
		t.Fatalf("Unexpected Error" + err.Error())
	}

	mockVerifier.AssertNumberOfCalls(t, "Verify", 1)
	mockUserLoginRepo.AssertNumberOfCalls(t, "GetUserLogin", 1)
	mockUserLoginRepo.AssertNumberOfCalls(t, "SaveUserLogin", 1)
}

func TestReturnErrorAfterFailedVerification(t *testing.T) {
	mockVerifier, mockUserLoginRepo, usecases := getMockedUseCases()
	_, err := usecases.GetUserLoginFromToken("fail_token")

	if err == nil {
		t.Fatalf("Expected an error" + err.Error())
	}

	mockVerifier.AssertNumberOfCalls(t, "Verify", 1)
	mockUserLoginRepo.AssertNumberOfCalls(t, "GetUserLogin", 0)
	mockUserLoginRepo.AssertNumberOfCalls(t, "SaveUserLogin", 0)
}
