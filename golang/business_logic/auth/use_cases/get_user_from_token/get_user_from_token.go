package get_user_from_token

import (
	"draw-on-animation/business-logic/auth/models/token_verifier"
	"draw-on-animation/business-logic/auth/models/user_login"
	"draw-on-animation/business-logic/repositories/models/user_logins"
	"strings"

	"github.com/samber/do"
)

func Invoke(injector *do.Injector, tokenToVerify string) (user_login.UserLogin, error) {

	token := strings.Replace(tokenToVerify, "Bearer ", "", 1)
	tokenVerifier := do.MustInvoke[token_verifier.TokenVerifier](injector)
	verificationResult, err := tokenVerifier.Verify(token)

	if err != nil {
		return user_login.UserLogin{}, err
	}

	userLoginRepo := do.MustInvoke[user_logins.UserLoginsRepo](injector)
	userLogin, found, err := userLoginRepo.GetUserLogin(verificationResult.Subject, verificationResult.SignInProvider)

	if err != nil {
		return user_login.UserLogin{}, err
	}

	if !found {
		userLogin = user_login.New(verificationResult.Subject, verificationResult.SignInProvider, verificationResult.Email)
		err = userLoginRepo.SaveUserLogin(userLogin)
		if err != nil {
			return user_login.UserLogin{}, err
		}
	}

	return userLogin, nil
}
