package auth

import (
	"draw-on-animation/business-logic/auth/models/user_login"
	"draw-on-animation/business-logic/auth/use_cases/get_user_from_token"

	"github.com/samber/do"
)

type UseCases struct {
	Injector *do.Injector
}

func NewUseCases() UseCases {
	return UseCases{
		Injector: getInjector(),
	}
}

func (self *UseCases) GetUserLoginFromToken(tokenToVerify string) (user_login.UserLogin, error) {
	return get_user_from_token.Invoke(self.Injector, tokenToVerify)
}
