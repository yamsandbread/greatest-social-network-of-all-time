package user_login

import (
	"time"

	"github.com/google/uuid"
)

type UserLogin struct {
	UserId         string
	Email          string
	ProviderUserId string
	ProviderName   string
	CreatedAt      string
}

func New(providerUserId, providerName, email string) UserLogin {
	return UserLogin{
		UserId:         uuid.NewString(),
		Email:          email,
		ProviderUserId: providerUserId,
		ProviderName:   providerName,
		CreatedAt:      time.Now().UTC().Format(time.RFC3339),
	}
}
