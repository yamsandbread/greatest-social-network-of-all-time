package token_verifier

import (
	"context"
	"draw-on-animation/business-logic/auth/models/auth_token_verification_result"
	"encoding/base64"
	"errors"
	"os"

	firebase "firebase.google.com/go"
	"google.golang.org/api/option"
)

type TokenVerifierFirebase struct{}

func (self *TokenVerifierFirebase) Verify(tokenToVerify string) (auth_token_verification_result.AuthTokenVerificationResult, error) {
	if tokenToVerify == "" {
		return auth_token_verification_result.AuthTokenVerificationResult{}, errors.New("No auth token")
	}

	value, ok := os.LookupEnv("FIREBASE_ADMIN_SDK_CONFIG")
	if !ok {
		return auth_token_verification_result.AuthTokenVerificationResult{}, errors.New("FIREBASE_ADMIN_SDK_CONFIG env variable is missing.")
	}

	parsedValue, err := base64.StdEncoding.DecodeString(value)
	if err != nil {
		return auth_token_verification_result.AuthTokenVerificationResult{}, err
	}

	opts := option.WithCredentialsJSON(parsedValue)
	ctx := context.Background()
	app, err := firebase.NewApp(ctx, nil, opts)
	if err != nil {
		return auth_token_verification_result.AuthTokenVerificationResult{}, err
	}

	client, err := app.Auth(context.Background())
	if err != nil {
		return auth_token_verification_result.AuthTokenVerificationResult{}, err
	}

	token, err := client.VerifyIDToken(ctx, tokenToVerify)
	if err != nil {
		return auth_token_verification_result.AuthTokenVerificationResult{}, err
	}

	email, ok := token.Claims["email"].(string)
	signInProvider, ok := token.Claims["firebase"].(map[string]interface{})["sign_in_provider"].(string)

	return auth_token_verification_result.AuthTokenVerificationResult{
		Subject:        token.Subject,
		Email:          email,
		SignInProvider: signInProvider,
	}, nil
}
