package token_verifier

import (
	"draw-on-animation/business-logic/auth/models/auth_token_verification_result"

	"github.com/stretchr/testify/mock"
)

type TokenVerifierMock struct {
	mock.Mock
}

func (self *TokenVerifierMock) Verify(tokenToVerify string) (auth_token_verification_result.AuthTokenVerificationResult, error) {
	args := self.Called(tokenToVerify)
	return args.Get(0).(auth_token_verification_result.AuthTokenVerificationResult), args.Error(1)
}
