package token_verifier

import "draw-on-animation/business-logic/auth/models/auth_token_verification_result"

type TokenVerifier interface {
	Verify(tokenToVerify string) (auth_token_verification_result.AuthTokenVerificationResult, error)
}
