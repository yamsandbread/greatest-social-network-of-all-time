package auth_token_verification_result

type AuthTokenVerificationResult struct {
	Subject        string
	Email          string
	SignInProvider string
}
