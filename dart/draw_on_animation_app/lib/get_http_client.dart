import 'package:dio/dio.dart';
import 'package:draw_on_animation_app/app_bootstrap/bootstrap_bloc/bootstrap_bloc.dart';

class GetHttpClient {
  final String baseUrl;
  final BootstrapBloc bootstrapBloc;

  GetHttpClient({required this.baseUrl, required this.bootstrapBloc});

  Dio get client {
    final httpClient = Dio(BaseOptions(
      baseUrl: baseUrl,
    ));

    final addTokenInterceptor =
        QueuedInterceptorsWrapper(onRequest: (options, handler) async {
      if (bootstrapBloc.isLoggedIn()) {
        options.headers['Authorization'] =
            await bootstrapBloc.getProviderToken();
      }
      return handler.next(options);
    });
    httpClient.interceptors.add(addTokenInterceptor);

    return httpClient;
  }
}
