// This class is autogenerated using npm run generateClasses
// ignore_for_file: library_private_types_in_public_api

class InitializeUserResponse {
  final _InitializeUserResponseData data;
  InitializeUserResponse({
    required this.data,
  });
  factory InitializeUserResponse.fromMap(Map map) {
    return InitializeUserResponse(
      data: _InitializeUserResponseData.fromMap(map["data"]),
    );
  }
}

class _InitializeUserResponseData {
  final String id;
  final String nickName;
  final bool isNew;
  _InitializeUserResponseData({
    required this.id,
    required this.nickName,
    required this.isNew,
  });
  factory _InitializeUserResponseData.fromMap(Map map) {
    return _InitializeUserResponseData(
      id: map["id"],
      nickName: map["nickName"],
      isNew: map["isNew"],
    );
  }
}
