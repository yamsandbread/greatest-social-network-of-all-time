import 'package:bloc/bloc.dart';
import 'package:draw_on_animation_app/app_bootstrap/models/env_vars_service.dart';
import 'package:meta/meta.dart';

part 'env_vars_event.dart';
part 'env_vars_state.dart';

class EnvVarsBloc extends Bloc<EnvVarsEvent, EnvVarsState> {
  final EnvVarsService _envVarsService;

  EnvVarsBloc(this._envVarsService) : super(EnvVarsInitial()) {
    on<EnvVarsLoadedEvent>((event, emit) {
      emit(EnvVarsLoaded());
    });

    on<EnvVarsErrorEvent>((event, emit) {
      emit(EnvVarsError());
    });
  }

  Future<void> setup() async {
    try {
      await _envVarsService.setup();
      add(EnvVarsLoadedEvent());
    } catch (e) {
      add(EnvVarsErrorEvent());
    }
  }

  String getEnvVar(String name) {
    return _envVarsService.getVar(name);
  }
}
