part of 'env_vars_bloc.dart';

@immutable
abstract class EnvVarsState {}

class EnvVarsInitial extends EnvVarsState {}

class EnvVarsLoaded extends EnvVarsState {}

class EnvVarsError extends EnvVarsState {}