part of 'env_vars_bloc.dart';

@immutable
abstract class EnvVarsEvent {}

class EnvVarsLoadedEvent extends EnvVarsEvent {}

class EnvVarsErrorEvent extends EnvVarsEvent {}
