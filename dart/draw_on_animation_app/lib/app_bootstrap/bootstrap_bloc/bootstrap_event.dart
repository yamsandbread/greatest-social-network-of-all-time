part of 'bootstrap_bloc.dart';

@immutable
abstract class BootstrapEvent {}

class BootstrapLoadedEvent extends BootstrapEvent {}

class BootstrapErrorEvent extends BootstrapEvent {}
