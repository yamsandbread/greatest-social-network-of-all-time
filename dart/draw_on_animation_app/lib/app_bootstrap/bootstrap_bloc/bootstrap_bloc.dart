import 'package:bloc/bloc.dart';
import 'package:draw_on_animation_app/app_bootstrap/models/login_handler.dart';
import 'package:meta/meta.dart';

part 'bootstrap_event.dart';
part 'bootstrap_state.dart';

class BootstrapBloc extends Bloc<BootstrapEvent, BootstrapState> {
  final LoginHandler _loginHandler;

  BootstrapBloc(this._loginHandler) : super(BootstrapInitial()) {
    on<BootstrapLoadedEvent>((event, emit) {
      emit(BootstrapLoaded());
    });

    on<BootstrapErrorEvent>((event, emit) {
      emit(BootstrapError());
    });
  }

  Future<void> setup() async {
    try {
      await _loginHandler.setup();
      add(BootstrapLoadedEvent());
    } catch (e) {
      add(BootstrapErrorEvent());
    }
  }

  Future<String> getProviderToken() {
    return _loginHandler.getToken();
  }

  bool isLoggedIn() {
    return _loginHandler.isLoggedIn();
  }
}
