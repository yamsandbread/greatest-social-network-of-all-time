part of 'bootstrap_bloc.dart';

@immutable
abstract class BootstrapState {}

class BootstrapInitial extends BootstrapState {}

class BootstrapLoaded extends BootstrapState {}

class BootstrapError extends BootstrapState {}
