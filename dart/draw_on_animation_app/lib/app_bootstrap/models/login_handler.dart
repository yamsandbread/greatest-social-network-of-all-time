abstract class LoginHandler {
  Future<void> setup();
  Future<String> getToken();
  bool isLoggedIn();
}