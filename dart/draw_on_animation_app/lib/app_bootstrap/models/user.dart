class User {
  final String id;
  final String nickName;
  final bool isNew;

  User({
    required this.id,
    required this.nickName,
    required this.isNew,
  });
}
