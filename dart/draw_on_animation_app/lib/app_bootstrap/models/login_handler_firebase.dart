// ignore_for_file: depend_on_referenced_packages

import 'package:draw_on_animation_app/app_bootstrap/models/login_handler.dart';

import 'package:draw_on_animation_app/firebase_options.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_ui_oauth_google/firebase_ui_oauth_google.dart';
import 'package:firebase_auth/firebase_auth.dart' hide EmailAuthProvider;
import 'package:firebase_ui_auth/firebase_ui_auth.dart';

class LoginHandlerFirebase extends LoginHandler {
  final String _googleClientId;
  LoginHandlerFirebase(this._googleClientId);

  @override
  Future<void> setup() async {
    await Firebase.initializeApp(
        options: DefaultFirebaseOptions.currentPlatform);

    FirebaseUIAuth.configureProviders([
      GoogleProvider(clientId: _googleClientId),
    ]);
  }

  @override
  Future<String> getToken() {
    if (FirebaseAuth.instance.currentUser == null) {
      throw "Attempted to get token before signing in";
    }
    return FirebaseAuth.instance.currentUser!.getIdToken();
  }

  @override
  bool isLoggedIn() => FirebaseAuth.instance.currentUser != null;
}
