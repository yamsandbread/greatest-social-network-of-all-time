abstract class EnvVarsService {
  Future<void> setup();
  String getVar(String name);
}