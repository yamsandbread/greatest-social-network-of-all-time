import 'package:draw_on_animation_app/app_bootstrap/models/env_vars_service.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class EnvVarsServiceDotEnv extends EnvVarsService {
  @override
  Future<void> setup() async {
    await dotenv.load(fileName: ".env");
  }

  @override
  String getVar(String name) {
    return dotenv.get(name);
  }
}
