import 'package:draw_on_animation_app/app_bootstrap/models/user.dart';

abstract class UserRepo {
  Future<User> getUser();
}