import 'package:dio/dio.dart';
import 'package:draw_on_animation_app/app_bootstrap/models/user.dart';
import 'package:draw_on_animation_app/network_contracts/initialize_user_response.dart';
import 'package:draw_on_animation_app/repositories/user_repo/user_repo.dart';

class UserRepoHttp extends UserRepo {
  final Dio _client;
  UserRepoHttp(this._client);

  @override
  Future<User> getUser() async {
    Response<Map<String, dynamic>> response =
        await _client.post("/initialize-user");

    var parsed = InitializeUserResponse.fromMap(response.data!);

    return User(
      id: parsed.data.id,
      isNew: parsed.data.isNew,
      nickName: parsed.data.nickName,
    );
  }
}
