import 'dart:developer';

import 'package:draw_on_animation_app/app_bootstrap/bootstrap_bloc/bootstrap_bloc.dart';
import 'package:draw_on_animation_app/app_bootstrap/env_vars_bloc/env_vars_bloc.dart';
import 'package:draw_on_animation_app/app_bootstrap/models/env_vars_service_dotenv.dart';
import 'package:draw_on_animation_app/app_bootstrap/models/login_handler_firebase.dart';
import 'package:draw_on_animation_app/get_http_client.dart';
import 'package:draw_on_animation_app/repositories/user_repo/user_repo.dart';
import 'package:draw_on_animation_app/repositories/user_repo/user_repo_http.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:firebase_ui_auth/firebase_ui_auth.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    WidgetsFlutterBinding.ensureInitialized();

    return getWithLoadedEnvVars(
      context,
      builder: (context) => getWithBootstrap(
        context,
        builder: (context) => getWithRepositories(
          context,
          builder: (context) => MaterialApp(
            initialRoute: context.read<BootstrapBloc>().isLoggedIn()
                ? "/profile"
                : "/sign-in",
            routes: {
              "/": (context) {
                final isLoggedIn = context.read<BootstrapBloc>().isLoggedIn();
                return Scaffold(
                  body: Text("This is the main app! $isLoggedIn"),
                );
              },
              '/sign-in': (context) {
                return SignInScreen(
                  actions: [
                    AuthStateChangeAction<SignedIn>((context, state) {
                      context
                          .read<BootstrapBloc>()
                          .getProviderToken()
                          .then((token) => log("The token is $token"));

                      context
                          .read<UserRepo>()
                          .getUser()
                          .then((user) => log("We got a user!!! $user"));

                      Navigator.pushReplacementNamed(context, '/profile');
                    }),
                  ],
                );
              },
              '/profile': (context) {
                return ProfileScreen(
                  actions: [
                    SignedOutAction((context) {
                      Navigator.pushReplacementNamed(context, '/sign-in');
                    }),
                  ],
                );
              },
            },
          ),
        ),
      ),
    );
  }

  Widget getWithLoadedEnvVars(BuildContext context,
          {required Widget Function(BuildContext) builder}) =>
      BlocProvider(
        create: (context) => EnvVarsBloc(EnvVarsServiceDotEnv()),
        child: BlocBuilder<EnvVarsBloc, EnvVarsState>(
          builder: (context, state) {
            if (state is EnvVarsInitial) {
              context.read<EnvVarsBloc>().setup();
              return getLoadingMaterialApp();
            } else if (state is EnvVarsLoaded) {
              return builder(context);
            } else {
              return getErrorMaterialApp();
            }
          },
        ),
      );

  Widget getWithBootstrap(BuildContext context,
      {required Widget Function(BuildContext) builder}) {
    final clientId =
        context.read<EnvVarsBloc>().getEnvVar('FIREBASE_GOOGLE_CLIENT_ID');
    return BlocProvider(
      create: (context) => BootstrapBloc(LoginHandlerFirebase(clientId)),
      child: BlocBuilder<BootstrapBloc, BootstrapState>(
        builder: (context, state) {
          if (state is BootstrapInitial) {
            context.read<BootstrapBloc>().setup();
            return getLoadingMaterialApp();
          } else if (state is BootstrapLoaded) {
            return builder(context);
          } else {
            return getErrorMaterialApp();
          }
        },
      ),
    );
  }

  Widget getWithRepositories(BuildContext context,
      {required Widget Function(BuildContext) builder}) {
    final baseUrl = context.read<EnvVarsBloc>().getEnvVar('URL');
    final httpClient = GetHttpClient(
      baseUrl: baseUrl,
      bootstrapBloc: context.read<BootstrapBloc>(),
    ).client;
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<UserRepo>(
            create: (context) => UserRepoHttp(httpClient)),
      ],
      child: builder(context),
    );
  }

  Widget getLoadingMaterialApp() => MaterialApp(
        builder: (context, child) => const Scaffold(
          body: Center(
              child: CircularProgressIndicator(
            color: Colors.black,
          )),
        ),
      );

  Widget getErrorMaterialApp() => MaterialApp(
        builder: (context, child) => const Scaffold(
          body: Text("Something is not right here..."),
        ),
      );
}
