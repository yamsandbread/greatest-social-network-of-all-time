# draw_on_animation_app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Setting up Firebase

The steps in this link are mostly correct [Setup Firebase](https://firebase.google.com/docs/flutter/setup)

A couple of caveats. The project on Firebase console was already created, so the command to configure was `flutterfire configure -p draw-on-animation`

Also, if the global dart installations are not added to the PATH, a temporary solution would be to run `export PATH="$PATH":"$HOME/.pub-cache/bin"`

Keys need to be generated and placed into a key.properties file. If Android Studio is installed, the keytool can be found in `<path to android studio>/jre/bin/keytool`

The resulting SHA1 and SHA256 must also be added in the Firebase console in the Project Settings -> General under the SHA certificate fingerprints section