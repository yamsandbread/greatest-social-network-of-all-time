package main

import (
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

func main() {
	functionsDirectory := "./../../golang/lambda_functions"
	filepath.WalkDir(functionsDirectory, func(path string, info os.DirEntry, err error) error {
		if err != nil {
			log.Fatal(err)
		}

		if info.Name() == "go.mod" {
			log.Println("Building:", path)
			packagePath := strings.Replace(path, "/go.mod", "", 1)
			cmd := exec.Command("go", "build", "-ldflags", "-w", "-ldflags", "-s", "main.go")
			absolutePath, err := filepath.Abs(packagePath)
			if err != nil {
				log.Fatal(err)
			}
			cmd.Dir = absolutePath
			_, err = cmd.Output()

			if err != nil {
				log.Fatal(err)
			}
			log.Println("Completed:", path)
		}

		return nil
	})
}
