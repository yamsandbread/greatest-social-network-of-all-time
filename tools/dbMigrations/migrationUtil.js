const { DynamoDBClient, CreateTableCommand, ScanCommand, BatchWriteItemCommand } = require("@aws-sdk/client-dynamodb");
const { promisify } = require('util');
var fs = require('fs');
var path = require('path');
const exec = promisify(require('child_process').exec);
const readDirectory = promisify(fs.readdir);
const sleep = promisify(setTimeout);

(async () => {
    const migrationsPath = path.join(__dirname, 'migrations');
    const migrationsTableName = 'DB_MIGRATIONS';
    const client = new DynamoDBClient({});

    const createMigrationTable = async () => {
        const command = new CreateTableCommand({
            TableName: migrationsTableName,
            AttributeDefinitions: [
                {
                    AttributeName: 'fileName',
                    AttributeType: 'S'
                },
                {
                    AttributeName: 'insertedAt',
                    AttributeType: 'N'
                }
            ],
            KeySchema: [
                {
                    AttributeName: 'fileName',
                    KeyType: 'HASH'
                },
                {
                    AttributeName: 'insertedAt',
                    KeyType: 'RANGE'
                },
            ],
            ProvisionedThroughput: {
                ReadCapacityUnits: 1,
                WriteCapacityUnits: 1
            },
        })
        try {
            const result = await client.send(command);

            // Give the table a few seconds to initialize
            await sleep(10000);
        } catch (err) {
            console.log("Migration table not created, it already exists.");
        }
    }

    const getFileNamesInDB = async () => {
        const command = new ScanCommand({
            TableName: migrationsTableName
        });
        const results = await client.send(command);
        return results.Items.map(item => item.fileName.S) || [];
    }

    const getFileNamesInFolder = async () => {
        return readDirectory(migrationsPath, { encoding: 'utf8' });
    }

    const getNewMigrations = (migrationsInDB, migrationsInFolder) => {
        return migrationsInFolder.filter(fileName => {
            return !migrationsInDB.includes(fileName);
        });
    }

    const runFileMigrations = async (fullPaths) => {
        for (let i = 0; i < fullPaths.length; i++) {
            console.log(await exec(`node ${fullPaths[i]}`));
        }
    }

    const storeNewFileNamesInDB = async (newFileNames) => {
        const command = new BatchWriteItemCommand({
            RequestItems: {
                [migrationsTableName]: newFileNames.map(fileName => ({
                    PutRequest: {
                        Item: {
                            "fileName": { S: fileName },
                            "insertedAt": { N: `${Date.now()}` }
                        },
                    },
                })),
            }
        });

        const result = await client.send(command);
    }

    await createMigrationTable();
    const [migrationsInDB, migrationsInFolder] = await Promise.all([getFileNamesInDB(), getFileNamesInFolder()]);
    const migrations = getNewMigrations(migrationsInDB, migrationsInFolder);
    if (!migrations.length) {
        console.log("No new migrations detected.");
        return;
    }
    console.log("New DB migrations:", migrations);
    await runFileMigrations(migrations.map(fileName => `${migrationsPath}/${fileName}`));
    await storeNewFileNamesInDB(migrations.map(fileName => fileName));
})();

