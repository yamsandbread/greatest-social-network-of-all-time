const { DynamoDBClient, UpdateTableCommand } = require("@aws-sdk/client-dynamodb");

(async () => {
    const client = new DynamoDBClient({});

    const command = new UpdateTableCommand({
        TableName: 'POSTS',
        AttributeDefinitions: [
            {
                AttributeName: 'UserId',
                AttributeType: 'S'
            },
            {
                AttributeName: 'CreatedAt',
                AttributeType: 'S'
            },
            {
                AttributeName: 'Status',
                AttributeType: 'S'
            },
        ],
        GlobalSecondaryIndexUpdates: [
            {
                Create: {
                    IndexName: 'StatusIndex',
                    KeySchema: [
                        {
                            AttributeName: 'Status',
                            KeyType: 'HASH',
                        }
                    ],
                    Projection: {
                        ProjectionType: 'ALL'
                    },
                    ProvisionedThroughput: {
                        ReadCapacityUnits: 10,
                        WriteCapacityUnits: 10,
                    },
                },
            }
        ]
    })
    try {
        const result = await client.send(command);
        console.log(result);
    } catch (err) {
        throw err;
    }
})();
