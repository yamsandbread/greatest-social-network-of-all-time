const { DynamoDBClient, CreateTableCommand } = require("@aws-sdk/client-dynamodb");

(async () => {
    const client = new DynamoDBClient({});

    const command = new CreateTableCommand({
        TableName: 'POSTS',
        KeySchema: [
            {
                AttributeName: 'UserId',
                KeyType: 'HASH'
            },
            {
                AttributeName: 'CreatedAt',
                KeyType: 'RANGE'
            },
        ],
        AttributeDefinitions: [
            {
                AttributeName: 'UserId',
                AttributeType: 'S'
            },
            {
                AttributeName: 'CreatedAt',
                AttributeType: 'S'
            }
        ],
        ProvisionedThroughput: {
            ReadCapacityUnits: 10,
            WriteCapacityUnits: 10
        },
    })
    try {
        const result = await client.send(command);
        console.log(result);
    } catch (err) {
        throw err;
    }
})();
