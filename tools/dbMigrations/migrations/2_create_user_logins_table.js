const { DynamoDBClient, CreateTableCommand } = require("@aws-sdk/client-dynamodb");

(async () => {
    const client = new DynamoDBClient({});

    const command = new CreateTableCommand({
        TableName: 'USER_LOGINS',
        KeySchema: [
            {
                AttributeName: 'ProviderUserId',
                KeyType: 'HASH'
            },
            {
                AttributeName: 'ProviderName',
                KeyType: 'RANGE'
            },
        ],
        AttributeDefinitions: [
            {
                AttributeName: 'ProviderUserId',
                AttributeType: 'S'
            },
            {
                AttributeName: 'ProviderName',
                AttributeType: 'S'
            },
            {
                AttributeName: 'UserId',
                AttributeType: 'S'
            },
        ],
        GlobalSecondaryIndexes: [
            {
                IndexName: 'UserIdIndex',
                KeySchema: [
                    {
                        AttributeName: 'UserId',
                        KeyType: 'HASH',
                    }
                ],
                Projection: {
                    ProjectionType: 'ALL'
                },
                ProvisionedThroughput: {
                    ReadCapacityUnits: 10,
                    WriteCapacityUnits: 10,
                },
            },
        ],
        ProvisionedThroughput: {
            ReadCapacityUnits: 10,
            WriteCapacityUnits: 10
        },
    })
    try {
        const result = await client.send(command);
        console.log(result);
    } catch (err) {
        throw err;
    }
})();
