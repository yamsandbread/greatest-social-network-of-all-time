const { DynamoDBClient, CreateTableCommand } = require("@aws-sdk/client-dynamodb");

(async () => {
    const client = new DynamoDBClient({});

    const command = new CreateTableCommand({
        TableName: 'TESTS',
        AttributeDefinitions: [
            {
                AttributeName: 'name',
                AttributeType: 'S'
            }
        ],
        KeySchema: [
            {
                AttributeName: 'name',
                KeyType: 'HASH'
            },
        ],
        ProvisionedThroughput: {
            ReadCapacityUnits: 1,
            WriteCapacityUnits: 1
        },
    })
    try {
        const result = await client.send(command);
        console.log(result);
    } catch (err) {
        console.error("Error running command: ", err);
    }


})();

