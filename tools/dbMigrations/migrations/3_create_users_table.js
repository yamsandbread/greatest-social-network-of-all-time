const { DynamoDBClient, CreateTableCommand } = require("@aws-sdk/client-dynamodb");

(async () => {
    const client = new DynamoDBClient({});

    const command = new CreateTableCommand({
        TableName: 'USERS',
        KeySchema: [
            {
                AttributeName: 'Id',
                KeyType: 'HASH'
            }
        ],
        AttributeDefinitions: [
            {
                AttributeName: 'Id',
                AttributeType: 'S'
            }
        ],
        ProvisionedThroughput: {
            ReadCapacityUnits: 10,
            WriteCapacityUnits: 10
        },
    })
    try {
        const result = await client.send(command);
        console.log(result);
    } catch (err) {
        throw err;
    }
})();
