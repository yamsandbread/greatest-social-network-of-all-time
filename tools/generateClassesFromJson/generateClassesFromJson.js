const { promisify } = require('util');
let ejs = require('ejs');
var fs = require('fs');
var path = require('path');
const exec = promisify(require('child_process').exec);
const readDirectory = promisify(fs.readdir);
const readFile = promisify(fs.readFile);

const getFileNameFromPath = (path) => {
    const fileNameChunks = path.split("/");
    return fileNameChunks[fileNameChunks.length - 1].replace('.json', '');
}

const parseTemplate = (templateString, schema, schemaPath) => {
    const name = getFileNameFromPath(schemaPath);
    return ejs.render(templateString, { schema, name, dartTypeExpander });
}

const getFiles = (path) => {
    return readDirectory(path, { encoding: 'utf8' }).then(files => {
        return files.map(file => `${path}/${file}`)
    })
}

const getFileContent = (path) => {
    return readFile(path, { encoding: 'utf8' });
}

const getFolderPaths = () => {
    const schemasPath = path.join(__dirname, 'schemas');
    const templatesPath = path.join(__dirname, 'templates');

    return {
        schemasPath,
        templatesPath
    }
}

const getSnakeCase = (fileName) => {
    // replace all uppercase letters with an `_` and the letter itself, then replace the `_` at the start, then make the result lowercase
    return fileName.replace(/([A-Z])/g, "_$1").replace(/^_/, '').toLowerCase();
}

const getFileNameFromSchemaAndTemplatePath = (schemaPath, templatePath) => {
    const fileName = getFileNameFromPath(schemaPath);

    const snakeCaseFileName = getSnakeCase(fileName);
    if (templatePath.endsWith("go.template")) {
        return path.join(__dirname, '..', '..', 'golang', 'network_contracts', `${snakeCaseFileName}.go`)
    } else if (templatePath.endsWith("dart.template")) {
        return path.join(__dirname, '..', '..', 'dart', 'draw_on_animation_app', 'lib', 'network_contracts', `${snakeCaseFileName}.dart`)
    } else if (templatePath.endsWith("ts.template")) {
        return path.join(__dirname, '..', '..', 'js', 'network_contracts', `${snakeCaseFileName}.ts`)
    }

    return undefined
}

const saveFile = (fileContent, path) => {
    fs.writeFileSync(path, fileContent)
}

const formatGeneratedFiles = async () => {
    const goGeneratedFilePath = path.join(__dirname, '..', '..', 'golang', 'network_contracts');
    const goFormattingPromise = exec(`cd ${goGeneratedFilePath} && go fmt .`);

    const dartGeneratedFilePath = path.join(__dirname, '..', '..', 'dart', 'draw_on_animation_app', 'lib', 'network_contracts');
    const dartFormattingPromise = exec(`cd ${dartGeneratedFilePath} && flutter format .`);

    const tsGeneratedFilePath = path.join(__dirname, '..', '..', 'js', 'network_contracts');
    const tsFormattingPromise = exec(`cd ${tsGeneratedFilePath} && npm run prettier`);

    await Promise.all([goFormattingPromise, dartFormattingPromise, tsFormattingPromise]);
}

const main = async () => {
    const {
        schemasPath,
        templatesPath
    } = getFolderPaths();

    const schemasPromise = getFiles(schemasPath);
    const templatesPromise = getFiles(templatesPath);
    const [schemaPaths, templatePaths] = await Promise.all([schemasPromise, templatesPromise]);

    const [templates, schemas] = await Promise.all([
        Promise.all(templatePaths.map(path => getFileContent(path))),
        Promise.all(schemaPaths.map(path => getFileContent(path).then(content => JSON.parse(content))))
    ])

    schemas.forEach((schema, schemaIndex) => {
        templates.forEach((template, templateIndex) => {
            const result = parseTemplate(template, schema, schemaPaths[schemaIndex]);
            const resultPath = getFileNameFromSchemaAndTemplatePath(schemaPaths[schemaIndex], templatePaths[templateIndex])
            if (resultPath) saveFile(result, resultPath)
        });
    });

    await addTsExports(schemaPaths);
    await formatGeneratedFiles();
}

const addTsExports = async (schemaPaths) => {
    const tsExportIndex = path.join(__dirname, '..', '..', 'js', 'network_contracts', 'index.ts');

    const schemaExports = schemaPaths.map(schemaPath => {
        const fileName = getFileNameFromPath(schemaPath);
        const exportName = getSnakeCase(fileName);
        return `export * from './${exportName}';`;
    }).join("\n");

    return saveFile(schemaExports, tsExportIndex);
}


// Since Dart does not support anonymous structures or inline classes, a JSON does not easily map to the desired output,
// as we need a separate class for each nested object.
// For Dart, each property is needed in three places:
// - In the class properties (before the constructor)
// - In the constructor (as a `required this.property` declaration)
// - In the fromMap factory constructor
// 
// This function flattens the JSON. It loops over an array, and when it encounters an object property,
// it pushes it to that same array, so that it can revisit this object and flatten it as well.
const dartTypeExpander = (classesToExpand) => {
    const normalized = [];
    const primitivesNameMap = {
        "int": "int",
        "double": "double",
        "boolean": "bool",
        "string": "String",
    }
    while (classesToExpand.length) {
        const schemaObjectAndName = classesToExpand.shift();
        const newSchemaObject = {};
        Object.keys(schemaObjectAndName.schemaObject).forEach((key) => {
            const value = schemaObjectAndName.schemaObject[key];
            const tempName = `_${schemaObjectAndName.name}${key.charAt(0).toUpperCase() + key.substring(1)}`.replace("__", "_");
            if (value === Object(value) && !Array.isArray(value)) {
                newSchemaObject[key] = {
                    type: ["object"],
                    name: tempName
                };
                classesToExpand.push({
                    name: tempName,
                    schemaObject: value
                });
            } else if (Array.isArray(value)) {
                let itemValue = value;
                newSchemaObject[key] = {
                    type: [],
                };
                while (Array.isArray(itemValue)) {
                    newSchemaObject[key].type.push("array");
                    itemValue = itemValue[0];
                }

                if (itemValue === Object(itemValue)) {
                    newSchemaObject[key].name = tempName;
                    newSchemaObject[key].type.push("object");
                    classesToExpand.push({
                        name: tempName,
                        schemaObject: itemValue
                    });
                } else {
                    newSchemaObject[key].type.push("primitive");
                    newSchemaObject[key].name = primitivesNameMap[itemValue];
                }
            } else {
                newSchemaObject[key] = {
                    type: ["primitive"],
                    name: primitivesNameMap[value]
                };
            }
        });
        normalized.push({ name: schemaObjectAndName.name, schemaObject: newSchemaObject });
    }
    return normalized;
}


main();