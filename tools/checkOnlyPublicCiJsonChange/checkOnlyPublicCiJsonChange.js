const { promisify } = require('util');
const exec = promisify(require('child_process').exec);

const getPathsOfChangedFiles = async () => {
    const source = process.env.CI_COMMIT_SHA;
    const targetBranch = `origin/${process.env.CI_MERGE_REQUEST_TARGET_BRANCH_NAME}`
    const filesChanged = await exec(`git diff --name-status ${targetBranch} ${source} `);

    // the status line will be a string starting with a letter indicating the status, then a tab (\t), then the file name
    return filesChanged.stdout.split("\n").filter(x => x).map(statusLine => {
        const [status, fileName] = statusLine.split("\t", 2);
        return {
            status,
            fileName
        }
    });
}

const main = async () => {
    console.log("Checking changed files")
    const changedFiles = await getPathsOfChangedFiles();

    for (let i = 0; i < changedFiles.length; i++) {
        const changedFile = changedFiles[i];
        if (!/^golang\/lambda_functions\/public_ci\/public_json\/[^\/]+\.json$/.test(changedFile.fileName)) {
            let msg = "ERROR: You played with files you're not supposed to play with.";
            msg += "\nKeep you changes restricted to only adding a JSON file in golang/lambda_functions/public_ci/public_json"
            msg += `\nInvalid file change: ${changedFile.fileName}\n`
            console.log(msg);
            throw msg;
        }

        if (changedFile.status !== "A") {
            let msg = "ERROR: You can only ADD a JSON file. You cannot modify or delete a file.";
            msg += "\nKeep you changes restricted to only adding a JSON file in golang/lambda_functions/public_ci/public_json"
            msg += `\nInvalid file change: ${changedFile.fileName}\n`
            console.log(msg);
            throw msg;
        }
    }

    if (changedFiles.length !== 1) {
        let msg = "ERROR: You can only add 1 JSON file at a time. Instead, you went wild and added all of these:\n";
        msg += changedFiles.map(changedFile => changedFile.fileName).join("\n")
        console.log(msg);
        throw msg;
    }

    console.log("Done checking file changes");
}

main();
