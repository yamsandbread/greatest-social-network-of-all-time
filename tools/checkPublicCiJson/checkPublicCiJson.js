const { promisify } = require('util');
var fs = require('fs');
var path = require('path');
const readDirectory = promisify(fs.readdir);
const readFile = promisify(fs.readFile);

const getFiles = (path) => {
    return readDirectory(path, { encoding: 'utf8' }).then(files => {
        return files.map(file => `${path}/${file}`)
    })
}

const getFileContent = (path) => {
    return readFile(path, { encoding: 'utf8' });
}

const validateFileContent = (filePath, fileContent) => {
    if (fileContent.length > 3000) {
        throw `File too large. Get rid of all the fluff in: ${filePath}`;
    }

    try {
        JSON.parse(fileContent);
    } catch (error) {
        throw `Make sure you are uploading a JSON. The file you uploaded could not be parsed as a JSON: ${filePath}`;
    }
}

const main = async () => {
    console.log("Validating JSON formats")
    const jsonPath = path.join(__dirname, '..', '..', 'golang', 'lambda_functions', 'public_ci', 'public_json');
    const filePaths = await getFiles(jsonPath);
    const fileContents = await Promise.all(filePaths.map(filePath => getFileContent(filePath)));

    for (let i = 0; i < filePaths.length; i++) {
        console.log(`Checking ${filePaths[i].split('/').at(-1)}`);
        validateFileContent(filePaths[i], fileContents[i]);
    }
    console.log("All JSON files are");

}

main();
